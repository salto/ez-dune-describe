[![pipeline status](https://gitlab.inria.fr/salto/ez-dune-describe/badges/main/pipeline.svg)](https://gitlab.inria.fr/salto/ez-dune-describe/-/commits/main)

# ez dune describe

This library contains a set of functions to easily retrieve the output
of `dune describe workspace`, as provided by the
[dune](https://github.com/ocaml/dune) build system for
[OCaml](https://ocaml.org) projects, and to iterate over all the
sources of an OCaml project.

Documentation is available [here](https://salto.gitlabpages.inria.fr/ez-dune-describe/ez_dune_describe/index.html).
