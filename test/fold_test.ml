open Ez_dune_describe
open Datatypes
module S = Set.Make (String)

let print =
  let open Datatypes.Compilable in
  function Exe e -> String.concat ", " e.names | Lib l -> l.uid

let test_file file expected_value =
  match
    match
      Csexp.input
      @@ open_in Filename.(concat parent_dir_name @@ concat "examples" file)
    with
    | Ok csexp -> Sexp_decode.run Decoder.workspace_decoder csexp
    | Error _ -> assert false
  with
  | None -> assert false
  | Some w ->
      List.map print @@ Workspace.foldCompilables (fun c cl -> c :: cl) w []
      = expected_value

let%test _ =
  test_file "coral"
    [ "cfa";
      "scrape";
      "cfa";
      "generate_dune";
      "generate_bench";
      "simpl";
      "scrape";
      "stableRels";
      "generate_dune";
      "945f43d33a21ec7b3483121af5ffb461";
      "615df946f70b6559f190a6d4351dcff7";
      "037cdf3087091f55e0ba1f493c76d6bc";
      "99972417c8587837b38682f8b5fee6e8";
      "f9e670164f5c3b47e6412545e89e90b3";
      "ae2633fbba7cfd1fe90fdf13d1a6b266";
      "267ad208f39d68c3fe98b47f54e130cd";
      "2c61db8e94cb08e0fe642152aee8121a";
      "a1e9636ad8f79edd763e5d9155a82e34";
      "9fe03ca268f7a71719c95f2542c21e8f";
      "ba85adfb1c97e7d7af3df35b16b2fc0d";
      "d903aaf296383e8fadbab09fad77d793";
      "2e8014935baedf5ffd10d94dc9659185";
      "911c556d3b0ee62de3a0f35c494f5d49";
      "5014e215e204cf8da6c32644cda1b31e";
      "339a601b8fe15c893c69ea594e7485b4";
      "bbfd41e93c2a548bb70e80ff50204110";
      "780b363f93badaa2c9e3caceb5f46835";
      "a2cb0c065e4b9aa29ebf1259c0b225b6";
      "498b5dbf60a477f5d2322629ef7bb03f";
      "1f2b5eb300ea716920494385a31bb5fb";
      "552d167dfa44ce4e498bf7878001e414";
      "5bc96b204874dbb27bda6571c5b5c9a7";
      "52603f9930efcca61c8e7216acb48efa";
      "69b0916f09495cc8be752e83ba603480";
      "5ae836dcdead11d5c16815297c5a1ae6";
      "45c1801133a53c9543a1b42200314e05";
      "78de867ef1c43a82d19e5ad5c40b808c";
      "f583e15ba6e7ff0993230b23a798127e";
      "9f3c74fef7e4be5cad139b33c5b47f08";
      "e2f5526b9354a4f612f1d6d14ec078e3";
      "c480a7c584d174c22d86dbdb79515d7d";
      "c9367091ddd9a70d99fc22ede348f17c";
      "53e93ede3ff1d58f32a924f44fbecefd";
      "1cf0deccef46439d999a2459e92982d3";
      "f3c6d7337a102ddcfa701a3f9b483761";
      "3f673b912ad580253cc7c3c0c863a93a";
      "2363fd46dac995a1c79679dfa1a9881b";
      "9370241364a9b030635f191fe949b911";
      "e68a558facd1546b51c7abdbf6aed1cb";
      "43b7cbe1f93f4f502ec614971027cff9";
      "24f4eb12e3ff51b310dbf7443c6087be";
      "32800212772915a976a5eacd2d0fcb72";
      "a1ddd16d653f58b7d989a95af129ee51";
      "ca6cc2b0189077cce731d5327e658aa4";
      "092c274fe179bf76244efc6236abbb42";
      "449445be7a24ce51e119d57e9e255d3f";
      "249b2edaf3cc552a247667041bb5f015";
      "dba142cc6af5828a7a8384e821e16f58";
      "78124ee7ff07cadb98140aa287e78e3c";
      "7c82c98fd32b5f392f3ad8c940ef1e9e";
      "f598837dd662583c9d6f841b8586310d"
    ]

let%test _ =
  test_file "csexp"
    [ "cae2600322b2b8a67c297c353294d54d";
      "944581b51422e2148578565cb0425f31";
      "bd50825c4f12f6b9c392a28985698a62";
      "55b023c301c18e021a22384b996d66af";
      "e90d385812291a9fcde3d83613d76c4d";
      "a364c7b5d882efea8330804d0f2a154c";
      "66c5927ddd01193cdcfac8ce97e58e63";
      "08f2b871a278556c65a5e886ad6772ec";
      "38e359c1ff0967c18a9ebc9b981e5a9e";
      "7e5ca295bb33ccc8be62b7243d03ba47";
      "92685e7ac0dd1fa9cd96be221032349e";
      "2c61db8e94cb08e0fe642152aee8121a";
      "5f5174092107521c0d4ecf21084d43c7";
      "708bf5748829e3636236f5d8c610f430";
      "ba85adfb1c97e7d7af3df35b16b2fc0d";
      "46774e2b7a404647f12956126bd28f95";
      "5014e215e204cf8da6c32644cda1b31e";
      "b91de1a8b6b882f5b4726d5b1f2ece6e";
      "1f2b5eb300ea716920494385a31bb5fb";
      "c18dc1aff21f9ab14695aebc5e336ed9";
      "690760db8a30f8e8f4021797e13ede81";
      "5ae836dcdead11d5c16815297c5a1ae6";
      "475f353b2705e034b0287d7ffe9e5225";
      "3803213fe04d38cea3157f466b9a8747";
      "ce20dcd0c4bae81524a96662594adde2";
      "c9367091ddd9a70d99fc22ede348f17c";
      "2ac224c04fa61d226241d5394bcfc444";
      "73ad4e016c34da2f2d4a1cff930ac883";
      "2363fd46dac995a1c79679dfa1a9881b";
      "0a5ac475dc06cf90fd25efabd674a231";
      "e68a558facd1546b51c7abdbf6aed1cb";
      "2c6f959289bddfd3b3ada8f64a3ca5d8";
      "e98bd409a98e65033ef31f23d34c99c3";
      "20ba2ea90fd3f2c1e62d9cb321b1a9eb";
      "46e75006466e7a020139d86575978cb3";
      "43b7cbe1f93f4f502ec614971027cff9";
      "24f4eb12e3ff51b310dbf7443c6087be";
      "449445be7a24ce51e119d57e9e255d3f";
      "249b2edaf3cc552a247667041bb5f015"
    ]

let%test _ = test_file "dmap" [ "3f8408925c6dbf918f7bafe5eae466ac" ]

let%test _ =
  test_file "mfg"
    [ "generate_dune";
      "generate_dune";
      "mfg";
      "52603f9930efcca61c8e7216acb48efa";
      "03ade606c206fe893c3487759124ca1b";
      "c480a7c584d174c22d86dbdb79515d7d";
      "653371f37513115a526a4d67d1a45724";
      "c7b36f1326ae39dd97104fb95068e29f";
      "ca6cc2b0189077cce731d5327e658aa4";
      "7c82c98fd32b5f392f3ad8c940ef1e9e"
    ]
