open Ez_dune_describe
open Datatypes.Module
open Datatypes.Compilable
open Datatypes.Workspace
module S = Set.Make (String)

let m_equal x y =
  match (x, y) with
  | ( { name = nx;
        impl = mx;
        intf = fx;
        cmt = cx;
        cmti = ix;
        impl_deps = dx;
        intf_deps = sx
      },
      { name = ny;
        impl = my;
        intf = fy;
        cmt = cy;
        cmti = iy;
        impl_deps = dy;
        intf_deps = sy
      } ) ->
      nx = ny && mx = my && fx = fy && cx = cy && ix = iy && S.equal dx dy
      && S.equal sx sy

let c_equal x y =
  match (x, y) with
  | ( Exe { names = nx; requires = rx; modules = mx; include_dirs = ix },
      Exe { names = ny; requires = ry; modules = my; include_dirs = iy } ) ->
      nx = ny && S.equal rx ry && List.for_all2 m_equal mx my && ix = iy
  | ( Lib
        { name = nx;
          uid = ux;
          local = lx;
          requires = rx;
          source_dir = sx;
          modules = mx;
          include_dirs = ix
        },
      Lib
        { name = ny;
          uid = uy;
          local = ly;
          requires = ry;
          source_dir = sy;
          modules = my;
          include_dirs = iy
        } ) ->
      nx = ny && ux = uy && lx = ly && S.equal rx ry && sx = sy
      && List.for_all2 m_equal mx my
      && ix = iy
  | _ -> false

let w_equal x y =
  match (x, y) with
  | ( { root = rx; context = cx; compilables = lx },
      { root = ry; context = cy; compilables = ly } ) ->
      rx = ry && cx = cy && List.for_all2 c_equal lx ly

let test_file file expected_value =
  match
    match
      Csexp.input
      @@ open_in Filename.(concat parent_dir_name @@ concat "examples" file)
    with
    | Ok csexp -> Sexp_decode.run Decoder.workspace_decoder csexp
    | Error _ -> assert false
  with
  | None -> false
  | Some w -> w_equal w expected_value

let%test _ =
  test_file "coral"
    { root = ".";
      context = "_build/default";
      compilables =
        [ Lib
            { name = "uutf";
              uid = "f598837dd662583c9d6f841b8586310d";
              local = false;
              requires = S.empty;
              source_dir = "/home/pierre/.opam/4.10.0/lib/uutf";
              modules = [];
              include_dirs = [ "/home/pierre/.opam/4.10.0/lib/uutf" ]
            };
          Lib
            { name = "unix";
              uid = "7c82c98fd32b5f392f3ad8c940ef1e9e";
              local = false;
              requires = S.empty;
              source_dir = "/home/pierre/.opam/4.10.0/lib/ocaml";
              modules = [];
              include_dirs = [ "/home/pierre/.opam/4.10.0/lib/ocaml" ]
            };
          Lib
            { name = "unionFind";
              uid = "78124ee7ff07cadb98140aa287e78e3c";
              local = false;
              requires = S.empty;
              source_dir = "/home/pierre/.opam/4.10.0/lib/unionFind";
              modules = [];
              include_dirs = [ "/home/pierre/.opam/4.10.0/lib/unionFind" ]
            };
          Lib
            { name = "uchar";
              uid = "dba142cc6af5828a7a8384e821e16f58";
              local = false;
              requires = S.empty;
              source_dir = "/home/pierre/.opam/4.10.0/lib/uchar";
              modules = [];
              include_dirs = [ "/home/pierre/.opam/4.10.0/lib/uchar" ]
            };
          Lib
            { name = "tyxml.functor";
              uid = "a2cb0c065e4b9aa29ebf1259c0b225b6";
              local = false;
              requires =
                S.of_list
                  [ "f598837dd662583c9d6f841b8586310d";
                    "69b0916f09495cc8be752e83ba603480";
                    "092c274fe179bf76244efc6236abbb42"
                  ];
              source_dir = "/home/pierre/.opam/4.10.0/lib/tyxml/functor";
              modules = [];
              include_dirs = [ "/home/pierre/.opam/4.10.0/lib/tyxml/functor" ]
            };
          Lib
            { name = "tyxml";
              uid = "2e8014935baedf5ffd10d94dc9659185";
              local = false;
              requires =
                S.of_list
                  [ "f598837dd662583c9d6f841b8586310d";
                    "a2cb0c065e4b9aa29ebf1259c0b225b6";
                    "69b0916f09495cc8be752e83ba603480"
                  ];
              source_dir = "/home/pierre/.opam/4.10.0/lib/tyxml";
              modules = [];
              include_dirs = [ "/home/pierre/.opam/4.10.0/lib/tyxml" ]
            };
          Lib
            { name = "stdlib-shims";
              uid = "249b2edaf3cc552a247667041bb5f015";
              local = false;
              requires = S.empty;
              source_dir = "/home/pierre/.opam/4.10.0/lib/stdlib-shims";
              modules = [];
              include_dirs = [ "/home/pierre/.opam/4.10.0/lib/stdlib-shims" ]
            };
          Lib
            { name = "simplLib";
              uid = "339a601b8fe15c893c69ea594e7485b4";
              local = true;
              requires =
                S.of_list
                  [ "f583e15ba6e7ff0993230b23a798127e";
                    "552d167dfa44ce4e498bf7878001e414";
                    "53e93ede3ff1d58f32a924f44fbecefd";
                    "52603f9930efcca61c8e7216acb48efa";
                    "3f673b912ad580253cc7c3c0c863a93a"
                  ];
              source_dir = "_build/default/src/simpl/lib";
              modules =
                [ { name = "Typecheck";
                    impl = "_build/default/src/simpl/lib/typecheck.ml";
                    intf = "_build/default/src/simpl/lib/typecheck.mli";
                    cmt =
                      "_build/default/src/simpl/lib/.simplLib.objs/byte/simplLib__Typecheck.cmt";
                    cmti =
                      "_build/default/src/simpl/lib/.simplLib.objs/byte/simplLib__Typecheck.cmti";
                    impl_deps = S.of_list [ "Ast" ];
                    intf_deps = S.of_list [ "Ast" ]
                  };
                  { name = "Tests";
                    impl = "_build/default/src/simpl/lib/tests.ml";
                    intf = "_build/default/src/simpl/lib/tests.mli";
                    cmt =
                      "_build/default/src/simpl/lib/.simplLib.objs/byte/simplLib__Tests.cmt";
                    cmti =
                      "_build/default/src/simpl/lib/.simplLib.objs/byte/simplLib__Tests.cmti";
                    impl_deps = S.of_list [ "Typecheck"; "Parser"; "Driver" ];
                    intf_deps = S.empty
                  };
                  { name = "Parser";
                    impl = "_build/default/src/simpl/lib/parser.ml";
                    intf = "_build/default/src/simpl/lib/parser.mli";
                    cmt =
                      "_build/default/src/simpl/lib/.simplLib.objs/byte/simplLib__Parser.cmt";
                    cmti =
                      "_build/default/src/simpl/lib/.simplLib.objs/byte/simplLib__Parser.cmti";
                    impl_deps = S.of_list [ "Ast" ];
                    intf_deps = S.of_list [ "Ast" ]
                  };
                  { name = "Main";
                    impl = "_build/default/src/simpl/lib/main.ml";
                    intf = "_build/default/src/simpl/lib/main.mli";
                    cmt =
                      "_build/default/src/simpl/lib/.simplLib.objs/byte/simplLib__Main.cmt";
                    cmti =
                      "_build/default/src/simpl/lib/.simplLib.objs/byte/simplLib__Main.cmti";
                    impl_deps =
                      S.of_list
                        [ "Typecheck";
                          "Tests";
                          "Parser";
                          "Driver";
                          "Ast";
                          "AnalyzerRel2";
                          "AnalyzerRel"
                        ];
                    intf_deps = S.of_list [ "Ast" ]
                  };
                  { name = "Lexer";
                    impl = "_build/default/src/simpl/lib/lexer.ml";
                    intf = "";
                    cmt =
                      "_build/default/src/simpl/lib/.simplLib.objs/byte/simplLib__Lexer.cmt";
                    cmti = "";
                    impl_deps = S.of_list [ "Parser" ];
                    intf_deps = S.empty
                  };
                  { name = "Driver";
                    impl = "_build/default/src/simpl/lib/driver.ml";
                    intf = "_build/default/src/simpl/lib/driver.mli";
                    cmt =
                      "_build/default/src/simpl/lib/.simplLib.objs/byte/simplLib__Driver.cmt";
                    cmti =
                      "_build/default/src/simpl/lib/.simplLib.objs/byte/simplLib__Driver.cmti";
                    impl_deps = S.of_list [ "Parser"; "Lexer" ];
                    intf_deps = S.of_list [ "Parser" ]
                  };
                  { name = "Corr2";
                    impl = "_build/default/src/simpl/lib/Corr2.ml";
                    intf = "_build/default/src/simpl/lib/Corr2.mli";
                    cmt =
                      "_build/default/src/simpl/lib/.simplLib.objs/byte/simplLib__Corr2.cmt";
                    cmti =
                      "_build/default/src/simpl/lib/.simplLib.objs/byte/simplLib__Corr2.cmti";
                    impl_deps = S.empty;
                    intf_deps = S.empty
                  };
                  { name = "Ast";
                    impl = "_build/default/src/simpl/lib/ast.ml";
                    intf = "";
                    cmt =
                      "_build/default/src/simpl/lib/.simplLib.objs/byte/simplLib__Ast.cmt";
                    cmti = "";
                    impl_deps = S.empty;
                    intf_deps = S.empty
                  };
                  { name = "AnalyzerRel2";
                    impl = "_build/default/src/simpl/lib/analyzerRel2.ml";
                    intf = "_build/default/src/simpl/lib/analyzerRel2.mli";
                    cmt =
                      "_build/default/src/simpl/lib/.simplLib.objs/byte/simplLib__AnalyzerRel2.cmt";
                    cmti =
                      "_build/default/src/simpl/lib/.simplLib.objs/byte/simplLib__AnalyzerRel2.cmti";
                    impl_deps = S.of_list [ "Corr2"; "Ast" ];
                    intf_deps = S.of_list [ "Ast" ]
                  };
                  { name = "AnalyzerRel";
                    impl = "_build/default/src/simpl/lib/analyzerRel.ml";
                    intf = "_build/default/src/simpl/lib/analyzerRel.mli";
                    cmt =
                      "_build/default/src/simpl/lib/.simplLib.objs/byte/simplLib__AnalyzerRel.cmt";
                    cmti =
                      "_build/default/src/simpl/lib/.simplLib.objs/byte/simplLib__AnalyzerRel.cmti";
                    impl_deps = S.of_list [ "Ast" ];
                    intf_deps = S.of_list [ "Ast" ]
                  };
                  { name = "SimplLib";
                    impl = "_build/default/src/simpl/lib/simplLib.ml-gen";
                    intf = "";
                    cmt =
                      "_build/default/src/simpl/lib/.simplLib.objs/byte/simplLib.cmt";
                    cmti = "";
                    impl_deps = S.empty;
                    intf_deps = S.empty
                  }
                ];
              include_dirs =
                [ "_build/default/src/simpl/lib/.simplLib.objs/byte" ]
            };
          Lib
            { name = "sexplib0";
              uid = "449445be7a24ce51e119d57e9e255d3f";
              local = false;
              requires = S.empty;
              source_dir = "/home/pierre/.opam/4.10.0/lib/sexplib0";
              modules = [];
              include_dirs = [ "/home/pierre/.opam/4.10.0/lib/sexplib0" ]
            };
          Lib
            { name = "seq";
              uid = "092c274fe179bf76244efc6236abbb42";
              local = false;
              requires = S.empty;
              source_dir = "/home/pierre/.opam/4.10.0/lib/seq";
              modules = [];
              include_dirs = [ "/home/pierre/.opam/4.10.0/lib/seq" ]
            };
          Lib
            { name = "result";
              uid = "ca6cc2b0189077cce731d5327e658aa4";
              local = false;
              requires = S.empty;
              source_dir = "/home/pierre/.opam/4.10.0/lib/result";
              modules = [];
              include_dirs = [ "/home/pierre/.opam/4.10.0/lib/result" ]
            };
          Lib
            { name = "reactiveData";
              uid = "5bc96b204874dbb27bda6571c5b5c9a7";
              local = false;
              requires = S.of_list [ "a1ddd16d653f58b7d989a95af129ee51" ];
              source_dir = "/home/pierre/.opam/4.10.0/lib/reactiveData";
              modules = [];
              include_dirs = [ "/home/pierre/.opam/4.10.0/lib/reactiveData" ]
            };
          Lib
            { name = "react";
              uid = "a1ddd16d653f58b7d989a95af129ee51";
              local = false;
              requires = S.empty;
              source_dir = "/home/pierre/.opam/4.10.0/lib/react";
              modules = [];
              include_dirs = [ "/home/pierre/.opam/4.10.0/lib/react" ]
            };
          Lib
            { name = "re";
              uid = "69b0916f09495cc8be752e83ba603480";
              local = false;
              requires = S.of_list [ "092c274fe179bf76244efc6236abbb42" ];
              source_dir = "/home/pierre/.opam/4.10.0/lib/re";
              modules = [];
              include_dirs = [ "/home/pierre/.opam/4.10.0/lib/re" ]
            };
          Lib
            { name = "ralist";
              uid = "32800212772915a976a5eacd2d0fcb72";
              local = true;
              requires = S.empty;
              source_dir = "_build/default/src/misc/ralist";
              modules =
                [ { name = "Ralist";
                    impl = "_build/default/src/misc/ralist/ralist.ml";
                    intf = "_build/default/src/misc/ralist/ralist.mli";
                    cmt =
                      "_build/default/src/misc/ralist/.ralist.objs/byte/ralist.cmt";
                    cmti =
                      "_build/default/src/misc/ralist/.ralist.objs/byte/ralist.cmti";
                    impl_deps = S.empty;
                    intf_deps = S.empty
                  }
                ];
              include_dirs =
                [ "_build/default/src/misc/ralist/.ralist.objs/byte" ]
            };
          Lib
            { name = "ppxlib.traverse_builtins";
              uid = "24f4eb12e3ff51b310dbf7443c6087be";
              local = false;
              requires = S.empty;
              source_dir =
                "/home/pierre/.opam/4.10.0/lib/ppxlib/traverse_builtins";
              modules = [];
              include_dirs =
                [ "/home/pierre/.opam/4.10.0/lib/ppxlib/traverse_builtins" ]
            };
          Lib
            { name = "ppxlib.stdppx";
              uid = "5ae836dcdead11d5c16815297c5a1ae6";
              local = false;
              requires =
                S.of_list
                  [ "449445be7a24ce51e119d57e9e255d3f";
                    "249b2edaf3cc552a247667041bb5f015"
                  ];
              source_dir = "/home/pierre/.opam/4.10.0/lib/ppxlib/stdppx";
              modules = [];
              include_dirs = [ "/home/pierre/.opam/4.10.0/lib/ppxlib/stdppx" ]
            };
          Lib
            { name = "ppxlib.print_diff";
              uid = "43b7cbe1f93f4f502ec614971027cff9";
              local = false;
              requires = S.empty;
              source_dir = "/home/pierre/.opam/4.10.0/lib/ppxlib/print_diff";
              modules = [];
              include_dirs =
                [ "/home/pierre/.opam/4.10.0/lib/ppxlib/print_diff" ]
            };
          Lib
            { name = "ppxlib.astlib";
              uid = "5014e215e204cf8da6c32644cda1b31e";
              local = false;
              requires =
                S.of_list
                  [ "c9367091ddd9a70d99fc22ede348f17c";
                    "1f2b5eb300ea716920494385a31bb5fb"
                  ];
              source_dir = "/home/pierre/.opam/4.10.0/lib/ppxlib/astlib";
              modules = [];
              include_dirs = [ "/home/pierre/.opam/4.10.0/lib/ppxlib/astlib" ]
            };
          Lib
            { name = "ppxlib.ast";
              uid = "ba85adfb1c97e7d7af3df35b16b2fc0d";
              local = false;
              requires =
                S.of_list
                  [ "5014e215e204cf8da6c32644cda1b31e";
                    "249b2edaf3cc552a247667041bb5f015"
                  ];
              source_dir = "/home/pierre/.opam/4.10.0/lib/ppxlib/ast";
              modules = [];
              include_dirs = [ "/home/pierre/.opam/4.10.0/lib/ppxlib/ast" ]
            };
          Lib
            { name = "ppxlib";
              uid = "2c61db8e94cb08e0fe642152aee8121a";
              local = false;
              requires =
                S.of_list
                  [ "e68a558facd1546b51c7abdbf6aed1cb";
                    "ba85adfb1c97e7d7af3df35b16b2fc0d";
                    "5ae836dcdead11d5c16815297c5a1ae6";
                    "5014e215e204cf8da6c32644cda1b31e";
                    "449445be7a24ce51e119d57e9e255d3f";
                    "43b7cbe1f93f4f502ec614971027cff9";
                    "24f4eb12e3ff51b310dbf7443c6087be";
                    "249b2edaf3cc552a247667041bb5f015";
                    "2363fd46dac995a1c79679dfa1a9881b"
                  ];
              source_dir = "/home/pierre/.opam/4.10.0/lib/ppxlib";
              modules = [];
              include_dirs = [ "/home/pierre/.opam/4.10.0/lib/ppxlib" ]
            };
          Lib
            { name = "ppx_deriving.runtime";
              uid = "52603f9930efcca61c8e7216acb48efa";
              local = false;
              requires = S.of_list [ "ca6cc2b0189077cce731d5327e658aa4" ];
              source_dir = "/home/pierre/.opam/4.10.0/lib/ppx_deriving/runtime";
              modules = [];
              include_dirs =
                [ "/home/pierre/.opam/4.10.0/lib/ppx_deriving/runtime" ]
            };
          Lib
            { name = "ppx_deriving.ord";
              uid = "945f43d33a21ec7b3483121af5ffb461";
              local = false;
              requires =
                S.of_list
                  [ "c9367091ddd9a70d99fc22ede348f17c";
                    "ba85adfb1c97e7d7af3df35b16b2fc0d";
                    "99972417c8587837b38682f8b5fee6e8";
                    "2c61db8e94cb08e0fe642152aee8121a"
                  ];
              source_dir = "/home/pierre/.opam/4.10.0/lib/ppx_deriving/ord";
              modules = [];
              include_dirs =
                [ "/home/pierre/.opam/4.10.0/lib/ppx_deriving/ord" ]
            };
          Lib
            { name = "ppx_deriving.enum";
              uid = "615df946f70b6559f190a6d4351dcff7";
              local = false;
              requires =
                S.of_list
                  [ "c9367091ddd9a70d99fc22ede348f17c";
                    "ba85adfb1c97e7d7af3df35b16b2fc0d";
                    "99972417c8587837b38682f8b5fee6e8";
                    "2c61db8e94cb08e0fe642152aee8121a"
                  ];
              source_dir = "/home/pierre/.opam/4.10.0/lib/ppx_deriving/enum";
              modules = [];
              include_dirs =
                [ "/home/pierre/.opam/4.10.0/lib/ppx_deriving/enum" ]
            };
          Lib
            { name = "ppx_deriving.api";
              uid = "99972417c8587837b38682f8b5fee6e8";
              local = false;
              requires =
                S.of_list
                  [ "e68a558facd1546b51c7abdbf6aed1cb";
                    "ca6cc2b0189077cce731d5327e658aa4";
                    "c9367091ddd9a70d99fc22ede348f17c";
                    "ba85adfb1c97e7d7af3df35b16b2fc0d";
                    "2c61db8e94cb08e0fe642152aee8121a"
                  ];
              source_dir = "/home/pierre/.opam/4.10.0/lib/ppx_deriving/api";
              modules = [];
              include_dirs =
                [ "/home/pierre/.opam/4.10.0/lib/ppx_deriving/api" ]
            };
          Lib
            { name = "ppx_derivers";
              uid = "e68a558facd1546b51c7abdbf6aed1cb";
              local = false;
              requires = S.empty;
              source_dir = "/home/pierre/.opam/4.10.0/lib/ppx_derivers";
              modules = [];
              include_dirs = [ "/home/pierre/.opam/4.10.0/lib/ppx_derivers" ]
            };
          Lib
            { name = "pprint";
              uid = "9370241364a9b030635f191fe949b911";
              local = false;
              requires = S.empty;
              source_dir = "/home/pierre/.opam/4.10.0/lib/pprint";
              modules = [];
              include_dirs = [ "/home/pierre/.opam/4.10.0/lib/pprint" ]
            };
          Lib
            { name = "ocaml-compiler-libs.shadow";
              uid = "2363fd46dac995a1c79679dfa1a9881b";
              local = false;
              requires = S.empty;
              source_dir =
                "/home/pierre/.opam/4.10.0/lib/ocaml-compiler-libs/shadow";
              modules = [];
              include_dirs =
                [ "/home/pierre/.opam/4.10.0/lib/ocaml-compiler-libs/shadow" ]
            };
          Lib
            { name = "ocaml-compiler-libs.common";
              uid = "1f2b5eb300ea716920494385a31bb5fb";
              local = false;
              requires = S.of_list [ "c9367091ddd9a70d99fc22ede348f17c" ];
              source_dir =
                "/home/pierre/.opam/4.10.0/lib/ocaml-compiler-libs/common";
              modules = [];
              include_dirs =
                [ "/home/pierre/.opam/4.10.0/lib/ocaml-compiler-libs/common" ]
            };
          Lib
            { name = "location";
              uid = "3f673b912ad580253cc7c3c0c863a93a";
              local = true;
              requires = S.empty;
              source_dir = "_build/default/src/misc/location";
              modules =
                [ { name = "Location";
                    impl = "_build/default/src/misc/location/location.ml";
                    intf = "_build/default/src/misc/location/location.mli";
                    cmt =
                      "_build/default/src/misc/location/.location.objs/byte/location.cmt";
                    cmti =
                      "_build/default/src/misc/location/.location.objs/byte/location.cmti";
                    impl_deps = S.empty;
                    intf_deps = S.empty
                  }
                ];
              include_dirs =
                [ "_build/default/src/misc/location/.location.objs/byte" ]
            };
          Lib
            { name = "js_of_ocaml-tyxml";
              uid = "9fe03ca268f7a71719c95f2542c21e8f";
              local = false;
              requires =
                S.of_list
                  [ "a2cb0c065e4b9aa29ebf1259c0b225b6";
                    "a1ddd16d653f58b7d989a95af129ee51";
                    "5bc96b204874dbb27bda6571c5b5c9a7";
                    "498b5dbf60a477f5d2322629ef7bb03f";
                    "2e8014935baedf5ffd10d94dc9659185"
                  ];
              source_dir = "/home/pierre/.opam/4.10.0/lib/js_of_ocaml-tyxml";
              modules = [];
              include_dirs =
                [ "/home/pierre/.opam/4.10.0/lib/js_of_ocaml-tyxml" ]
            };
          Lib
            { name = "js_of_ocaml-ppx.as-lib";
              uid = "f9e670164f5c3b47e6412545e89e90b3";
              local = false;
              requires =
                S.of_list
                  [ "c9367091ddd9a70d99fc22ede348f17c";
                    "ba85adfb1c97e7d7af3df35b16b2fc0d";
                    "2c61db8e94cb08e0fe642152aee8121a"
                  ];
              source_dir =
                "/home/pierre/.opam/4.10.0/lib/js_of_ocaml-ppx/as-lib";
              modules = [];
              include_dirs =
                [ "/home/pierre/.opam/4.10.0/lib/js_of_ocaml-ppx/as-lib" ]
            };
          Lib
            { name = "js_of_ocaml-ppx";
              uid = "037cdf3087091f55e0ba1f493c76d6bc";
              local = false;
              requires = S.of_list [ "f9e670164f5c3b47e6412545e89e90b3" ];
              source_dir = "/home/pierre/.opam/4.10.0/lib/js_of_ocaml-ppx";
              modules = [];
              include_dirs = [ "/home/pierre/.opam/4.10.0/lib/js_of_ocaml-ppx" ]
            };
          Lib
            { name = "js_of_ocaml-compiler.runtime";
              uid = "f3c6d7337a102ddcfa701a3f9b483761";
              local = false;
              requires = S.empty;
              source_dir =
                "/home/pierre/.opam/4.10.0/lib/js_of_ocaml-compiler/runtime";
              modules = [];
              include_dirs =
                [ "/home/pierre/.opam/4.10.0/lib/js_of_ocaml-compiler/runtime" ]
            };
          Lib
            { name = "js_of_ocaml";
              uid = "498b5dbf60a477f5d2322629ef7bb03f";
              local = false;
              requires =
                S.of_list
                  [ "f3c6d7337a102ddcfa701a3f9b483761";
                    "e2f5526b9354a4f612f1d6d14ec078e3";
                    "dba142cc6af5828a7a8384e821e16f58"
                  ];
              source_dir = "/home/pierre/.opam/4.10.0/lib/js_of_ocaml";
              modules = [];
              include_dirs = [ "/home/pierre/.opam/4.10.0/lib/js_of_ocaml" ]
            };
          Lib
            { name = "jsDemo";
              uid = "267ad208f39d68c3fe98b47f54e130cd";
              local = true;
              requires =
                S.of_list
                  [ "9fe03ca268f7a71719c95f2542c21e8f";
                    "911c556d3b0ee62de3a0f35c494f5d49";
                    "498b5dbf60a477f5d2322629ef7bb03f"
                  ];
              source_dir = "_build/default/src/misc/jsDemo";
              modules =
                [ { name = "JsDemo";
                    impl = "_build/default/src/misc/jsDemo/jsDemo.ml";
                    intf = "_build/default/src/misc/jsDemo/jsDemo.mli";
                    cmt =
                      "_build/default/src/misc/jsDemo/.jsDemo.objs/byte/jsDemo.cmt";
                    cmti =
                      "_build/default/src/misc/jsDemo/.jsDemo.objs/byte/jsDemo.cmti";
                    impl_deps = S.empty;
                    intf_deps = S.empty
                  }
                ];
              include_dirs =
                [ "_build/default/src/misc/jsDemo/.jsDemo.objs/byte" ]
            };
          Lib
            { name = "inferno";
              uid = "552d167dfa44ce4e498bf7878001e414";
              local = false;
              requires =
                S.of_list
                  [ "9370241364a9b030635f191fe949b911";
                    "78124ee7ff07cadb98140aa287e78e3c"
                  ];
              source_dir = "/home/pierre/.opam/4.10.0/lib/inferno";
              modules = [];
              include_dirs = [ "/home/pierre/.opam/4.10.0/lib/inferno" ]
            };
          Lib
            { name = "ezjs_ace";
              uid = "911c556d3b0ee62de3a0f35c494f5d49";
              local = false;
              requires = S.of_list [ "498b5dbf60a477f5d2322629ef7bb03f" ];
              source_dir = "/home/pierre/.opam/4.10.0/lib/ezjs_ace";
              modules = [];
              include_dirs = [ "/home/pierre/.opam/4.10.0/lib/ezjs_ace" ]
            };
          Lib
            { name = "dmap";
              uid = "1cf0deccef46439d999a2459e92982d3";
              local = false;
              requires = S.empty;
              source_dir = "/home/pierre/.opam/4.10.0/lib/dmap";
              modules = [];
              include_dirs = [ "/home/pierre/.opam/4.10.0/lib/dmap" ]
            };
          Lib
            { name = "coral";
              uid = "53e93ede3ff1d58f32a924f44fbecefd";
              local = true;
              requires = S.empty;
              source_dir = "_build/default/src/coral";
              modules =
                [ { name = "Coral";
                    impl = "_build/default/src/coral/coral.ml";
                    intf = "_build/default/src/coral/coral.mli";
                    cmt = "_build/default/src/coral/.coral.objs/byte/coral.cmt";
                    cmti =
                      "_build/default/src/coral/.coral.objs/byte/coral.cmti";
                    impl_deps = S.empty;
                    intf_deps = S.empty
                  }
                ];
              include_dirs = [ "_build/default/src/coral/.coral.objs/byte" ]
            };
          Lib
            { name = "compiler-libs.common";
              uid = "c9367091ddd9a70d99fc22ede348f17c";
              local = false;
              requires = S.empty;
              source_dir = "/home/pierre/.opam/4.10.0/lib/ocaml/compiler-libs";
              modules = [];
              include_dirs =
                [ "/home/pierre/.opam/4.10.0/lib/ocaml/compiler-libs" ]
            };
          Lib
            { name = "cmdliner";
              uid = "c480a7c584d174c22d86dbdb79515d7d";
              local = false;
              requires = S.empty;
              source_dir = "/home/pierre/.opam/4.10.0/lib/cmdliner";
              modules = [];
              include_dirs = [ "/home/pierre/.opam/4.10.0/lib/cmdliner" ]
            };
          Lib
            { name = "bytes";
              uid = "e2f5526b9354a4f612f1d6d14ec078e3";
              local = false;
              requires = S.empty;
              source_dir = "/home/pierre/.opam/4.10.0/lib/ocaml";
              modules = [];
              include_dirs = [ "/home/pierre/.opam/4.10.0/lib/ocaml" ]
            };
          Lib
            { name = "Time";
              uid = "45c1801133a53c9543a1b42200314e05";
              local = true;
              requires = S.of_list [ "7c82c98fd32b5f392f3ad8c940ef1e9e" ];
              source_dir = "_build/default/src/misc/time";
              modules =
                [ { name = "Time";
                    impl = "_build/default/src/misc/time/time.ml";
                    intf = "_build/default/src/misc/time/time.mli";
                    cmt =
                      "_build/default/src/misc/time/.Time.objs/byte/time.cmt";
                    cmti =
                      "_build/default/src/misc/time/.Time.objs/byte/time.cmti";
                    impl_deps = S.empty;
                    intf_deps = S.empty
                  }
                ];
              include_dirs = [ "_build/default/src/misc/time/.Time.objs/byte" ]
            };
          Lib
            { name = "Scraper";
              uid = "9f3c74fef7e4be5cad139b33c5b47f08";
              local = true;
              requires = S.empty;
              source_dir = "_build/default/src/misc/scraper";
              modules =
                [ { name = "Scraper";
                    impl = "_build/default/src/misc/scraper/scraper.ml";
                    intf = "_build/default/src/misc/scraper/scraper.mli";
                    cmt =
                      "_build/default/src/misc/scraper/.Scraper.objs/byte/scraper.cmt";
                    cmti =
                      "_build/default/src/misc/scraper/.Scraper.objs/byte/scraper.cmti";
                    impl_deps = S.empty;
                    intf_deps = S.empty
                  }
                ];
              include_dirs =
                [ "_build/default/src/misc/scraper/.Scraper.objs/byte" ]
            };
          Lib
            { name = "Memo";
              uid = "bbfd41e93c2a548bb70e80ff50204110";
              local = true;
              requires =
                S.of_list
                  [ "f583e15ba6e7ff0993230b23a798127e";
                    "52603f9930efcca61c8e7216acb48efa";
                    "1cf0deccef46439d999a2459e92982d3"
                  ];
              source_dir = "_build/default/src/misc/memo";
              modules =
                [ { name = "Memo_";
                    impl = "_build/default/src/misc/memo/memo_.ml";
                    intf = "_build/default/src/misc/memo/memo_.mli";
                    cmt =
                      "_build/default/src/misc/memo/.Memo.objs/byte/memo__Memo_.cmt";
                    cmti =
                      "_build/default/src/misc/memo/.Memo.objs/byte/memo__Memo_.cmti";
                    impl_deps = S.empty;
                    intf_deps = S.empty
                  };
                  { name = "MemoFixN";
                    impl = "_build/default/src/misc/memo/memoFixN.ml";
                    intf = "_build/default/src/misc/memo/memoFixN.mli";
                    cmt =
                      "_build/default/src/misc/memo/.Memo.objs/byte/memo__MemoFixN.cmt";
                    cmti =
                      "_build/default/src/misc/memo/.Memo.objs/byte/memo__MemoFixN.cmti";
                    impl_deps = S.of_list [ "Memo_" ];
                    intf_deps = S.of_list [ "Memo_" ]
                  };
                  { name = "Memo";
                    impl = "_build/default/src/misc/memo/memo.ml";
                    intf = "";
                    cmt =
                      "_build/default/src/misc/memo/.Memo.objs/byte/memo.cmt";
                    cmti = "";
                    impl_deps = S.of_list [ "Memo_"; "MemoFixN" ];
                    intf_deps = S.empty
                  };
                  { name = "Memo__";
                    impl = "_build/default/src/misc/memo/memo__.ml-gen";
                    intf = "";
                    cmt =
                      "_build/default/src/misc/memo/.Memo.objs/byte/memo__.cmt";
                    cmti = "";
                    impl_deps = S.empty;
                    intf_deps = S.empty
                  }
                ];
              include_dirs = [ "_build/default/src/misc/memo/.Memo.objs/byte" ]
            };
          Lib
            { name = "Logs";
              uid = "f583e15ba6e7ff0993230b23a798127e";
              local = true;
              requires = S.empty;
              source_dir = "_build/default/src/misc/logs";
              modules =
                [ { name = "Logs";
                    impl = "_build/default/src/misc/logs/logs.ml";
                    intf = "_build/default/src/misc/logs/logs.mli";
                    cmt =
                      "_build/default/src/misc/logs/.Logs.objs/byte/logs.cmt";
                    cmti =
                      "_build/default/src/misc/logs/.Logs.objs/byte/logs.cmti";
                    impl_deps = S.empty;
                    intf_deps = S.empty
                  }
                ];
              include_dirs = [ "_build/default/src/misc/logs/.Logs.objs/byte" ]
            };
          Lib
            { name = "Lang";
              uid = "d903aaf296383e8fadbab09fad77d793";
              local = true;
              requires =
                S.of_list
                  [ "f583e15ba6e7ff0993230b23a798127e";
                    "bbfd41e93c2a548bb70e80ff50204110";
                    "52603f9930efcca61c8e7216acb48efa";
                    "3f673b912ad580253cc7c3c0c863a93a"
                  ];
              source_dir = "_build/default/src/cfa/lang";
              modules =
                [ { name = "Parser";
                    impl = "_build/default/src/cfa/lang/parser.ml";
                    intf = "_build/default/src/cfa/lang/parser.mli";
                    cmt =
                      "_build/default/src/cfa/lang/.Lang.objs/byte/lang__Parser.cmt";
                    cmti =
                      "_build/default/src/cfa/lang/.Lang.objs/byte/lang__Parser.cmti";
                    impl_deps = S.of_list [ "Ast" ];
                    intf_deps = S.of_list [ "Ast" ]
                  };
                  { name = "Lexer";
                    impl = "_build/default/src/cfa/lang/lexer.ml";
                    intf = "";
                    cmt =
                      "_build/default/src/cfa/lang/.Lang.objs/byte/lang__Lexer.cmt";
                    cmti = "";
                    impl_deps = S.of_list [ "Parser" ];
                    intf_deps = S.empty
                  };
                  { name = "Ast";
                    impl = "_build/default/src/cfa/lang/ast.ml";
                    intf = "";
                    cmt =
                      "_build/default/src/cfa/lang/.Lang.objs/byte/lang__Ast.cmt";
                    cmti = "";
                    impl_deps = S.empty;
                    intf_deps = S.empty
                  };
                  { name = "Lang";
                    impl = "_build/default/src/cfa/lang/lang.ml-gen";
                    intf = "";
                    cmt = "_build/default/src/cfa/lang/.Lang.objs/byte/lang.cmt";
                    cmti = "";
                    impl_deps = S.empty;
                    intf_deps = S.empty
                  }
                ];
              include_dirs = [ "_build/default/src/cfa/lang/.Lang.objs/byte" ]
            };
          Lib
            { name = "DuneTestGen";
              uid = "78de867ef1c43a82d19e5ad5c40b808c";
              local = true;
              requires = S.empty;
              source_dir = "_build/default/src/misc/dune_test_gen";
              modules =
                [ { name = "DuneTestGen";
                    impl =
                      "_build/default/src/misc/dune_test_gen/DuneTestGen.ml";
                    intf =
                      "_build/default/src/misc/dune_test_gen/DuneTestGen.mli";
                    cmt =
                      "_build/default/src/misc/dune_test_gen/.DuneTestGen.objs/byte/duneTestGen.cmt";
                    cmti =
                      "_build/default/src/misc/dune_test_gen/.DuneTestGen.objs/byte/duneTestGen.cmti";
                    impl_deps = S.empty;
                    intf_deps = S.empty
                  }
                ];
              include_dirs =
                [ "_build/default/src/misc/dune_test_gen/.DuneTestGen.objs/byte"
                ]
            };
          Lib
            { name = "CfaLib";
              uid = "ae2633fbba7cfd1fe90fdf13d1a6b266";
              local = true;
              requires =
                S.of_list
                  [ "f583e15ba6e7ff0993230b23a798127e";
                    "d903aaf296383e8fadbab09fad77d793";
                    "a1e9636ad8f79edd763e5d9155a82e34";
                    "780b363f93badaa2c9e3caceb5f46835";
                    "52603f9930efcca61c8e7216acb48efa"
                  ];
              source_dir = "_build/default/src/cfa/lib";
              modules =
                [ { name = "Main";
                    impl = "_build/default/src/cfa/lib/main.ml";
                    intf = "_build/default/src/cfa/lib/main.mli";
                    cmt =
                      "_build/default/src/cfa/lib/.CfaLib.objs/byte/cfaLib__Main.cmt";
                    cmti =
                      "_build/default/src/cfa/lib/.CfaLib.objs/byte/cfaLib__Main.cmti";
                    impl_deps = S.of_list [ "Driver" ];
                    intf_deps = S.empty
                  };
                  { name = "Driver";
                    impl = "_build/default/src/cfa/lib/driver.ml";
                    intf = "_build/default/src/cfa/lib/driver.mli";
                    cmt =
                      "_build/default/src/cfa/lib/.CfaLib.objs/byte/cfaLib__Driver.cmt";
                    cmti =
                      "_build/default/src/cfa/lib/.CfaLib.objs/byte/cfaLib__Driver.cmti";
                    impl_deps = S.empty;
                    intf_deps = S.empty
                  };
                  { name = "CfaLib";
                    impl = "_build/default/src/cfa/lib/cfaLib.ml-gen";
                    intf = "";
                    cmt =
                      "_build/default/src/cfa/lib/.CfaLib.objs/byte/cfaLib.cmt";
                    cmti = "";
                    impl_deps = S.empty;
                    intf_deps = S.empty
                  }
                ];
              include_dirs = [ "_build/default/src/cfa/lib/.CfaLib.objs/byte" ]
            };
          Lib
            { name = "Analyzers";
              uid = "a1e9636ad8f79edd763e5d9155a82e34";
              local = true;
              requires =
                S.of_list
                  [ "f583e15ba6e7ff0993230b23a798127e";
                    "d903aaf296383e8fadbab09fad77d793";
                    "780b363f93badaa2c9e3caceb5f46835";
                    "52603f9930efcca61c8e7216acb48efa";
                    "32800212772915a976a5eacd2d0fcb72"
                  ];
              source_dir = "_build/default/src/cfa/analyzers";
              modules =
                [ { name = "Uf";
                    impl = "_build/default/src/cfa/analyzers/uf.ml";
                    intf = "_build/default/src/cfa/analyzers/uf.mli";
                    cmt =
                      "_build/default/src/cfa/analyzers/.Analyzers.objs/byte/analyzers__Uf.cmt";
                    cmti =
                      "_build/default/src/cfa/analyzers/.Analyzers.objs/byte/analyzers__Uf.cmti";
                    impl_deps = S.empty;
                    intf_deps = S.empty
                  };
                  { name = "Two";
                    impl = "_build/default/src/cfa/analyzers/two.ml";
                    intf = "_build/default/src/cfa/analyzers/two.mli";
                    cmt =
                      "_build/default/src/cfa/analyzers/.Analyzers.objs/byte/analyzers__Two.cmt";
                    cmti =
                      "_build/default/src/cfa/analyzers/.Analyzers.objs/byte/analyzers__Two.cmti";
                    impl_deps = S.empty;
                    intf_deps = S.empty
                  };
                  { name = "NablaRec";
                    impl = "_build/default/src/cfa/analyzers/nablaRec.ml";
                    intf = "_build/default/src/cfa/analyzers/nablaRec.mli";
                    cmt =
                      "_build/default/src/cfa/analyzers/.Analyzers.objs/byte/analyzers__NablaRec.cmt";
                    cmti =
                      "_build/default/src/cfa/analyzers/.Analyzers.objs/byte/analyzers__NablaRec.cmti";
                    impl_deps = S.of_list [ "Uf"; "Two"; "NablaCommon"; "Fun" ];
                    intf_deps = S.of_list [ "NablaCommon" ]
                  };
                  { name = "NablaCommon";
                    impl = "_build/default/src/cfa/analyzers/nablaCommon.ml";
                    intf = "_build/default/src/cfa/analyzers/nablaCommon.mli";
                    cmt =
                      "_build/default/src/cfa/analyzers/.Analyzers.objs/byte/analyzers__NablaCommon.cmt";
                    cmti =
                      "_build/default/src/cfa/analyzers/.Analyzers.objs/byte/analyzers__NablaCommon.cmti";
                    impl_deps = S.of_list [ "Map2"; "Fun"; "Calls"; "Alarming" ];
                    intf_deps = S.of_list [ "Fun"; "Calls" ]
                  };
                  { name = "Nabla";
                    impl = "_build/default/src/cfa/analyzers/nabla.ml";
                    intf = "_build/default/src/cfa/analyzers/nabla.mli";
                    cmt =
                      "_build/default/src/cfa/analyzers/.Analyzers.objs/byte/analyzers__Nabla.cmt";
                    cmti =
                      "_build/default/src/cfa/analyzers/.Analyzers.objs/byte/analyzers__Nabla.cmti";
                    impl_deps = S.of_list [ "Two"; "NablaCommon"; "Fun" ];
                    intf_deps = S.of_list [ "NablaCommon" ]
                  };
                  { name = "Map2";
                    impl = "_build/default/src/cfa/analyzers/map2.ml";
                    intf = "_build/default/src/cfa/analyzers/map2.mli";
                    cmt =
                      "_build/default/src/cfa/analyzers/.Analyzers.objs/byte/analyzers__Map2.cmt";
                    cmti =
                      "_build/default/src/cfa/analyzers/.Analyzers.objs/byte/analyzers__Map2.cmti";
                    impl_deps = S.empty;
                    intf_deps = S.empty
                  };
                  { name = "GlobalIndependentAttribute";
                    impl =
                      "_build/default/src/cfa/analyzers/globalIndependentAttribute.ml";
                    intf =
                      "_build/default/src/cfa/analyzers/globalIndependentAttribute.mli";
                    cmt =
                      "_build/default/src/cfa/analyzers/.Analyzers.objs/byte/analyzers__GlobalIndependentAttribute.cmt";
                    cmti =
                      "_build/default/src/cfa/analyzers/.Analyzers.objs/byte/analyzers__GlobalIndependentAttribute.cmti";
                    impl_deps =
                      S.of_list [ "Two"; "GlobalCommon"; "Fun"; "Calls" ];
                    intf_deps = S.of_list [ "Calls" ]
                  };
                  { name = "GlobalCommon";
                    impl = "_build/default/src/cfa/analyzers/globalCommon.ml";
                    intf = "_build/default/src/cfa/analyzers/globalCommon.mli";
                    cmt =
                      "_build/default/src/cfa/analyzers/.Analyzers.objs/byte/analyzers__GlobalCommon.cmt";
                    cmti =
                      "_build/default/src/cfa/analyzers/.Analyzers.objs/byte/analyzers__GlobalCommon.cmti";
                    impl_deps = S.of_list [ "Map2"; "Calls"; "Alarming" ];
                    intf_deps = S.of_list [ "Calls" ]
                  };
                  { name = "Global";
                    impl = "_build/default/src/cfa/analyzers/global.ml";
                    intf = "_build/default/src/cfa/analyzers/global.mli";
                    cmt =
                      "_build/default/src/cfa/analyzers/.Analyzers.objs/byte/analyzers__Global.cmt";
                    cmti =
                      "_build/default/src/cfa/analyzers/.Analyzers.objs/byte/analyzers__Global.cmti";
                    impl_deps =
                      S.of_list [ "Two"; "GlobalCommon"; "Fun"; "Calls" ];
                    intf_deps = S.of_list [ "Calls" ]
                  };
                  { name = "Fun";
                    impl = "_build/default/src/cfa/analyzers/fun.ml";
                    intf = "_build/default/src/cfa/analyzers/fun.mli";
                    cmt =
                      "_build/default/src/cfa/analyzers/.Analyzers.objs/byte/analyzers__Fun.cmt";
                    cmti =
                      "_build/default/src/cfa/analyzers/.Analyzers.objs/byte/analyzers__Fun.cmti";
                    impl_deps = S.empty;
                    intf_deps = S.empty
                  };
                  { name = "Calls";
                    impl = "_build/default/src/cfa/analyzers/calls.ml";
                    intf = "_build/default/src/cfa/analyzers/calls.mli";
                    cmt =
                      "_build/default/src/cfa/analyzers/.Analyzers.objs/byte/analyzers__Calls.cmt";
                    cmti =
                      "_build/default/src/cfa/analyzers/.Analyzers.objs/byte/analyzers__Calls.cmti";
                    impl_deps = S.empty;
                    intf_deps = S.empty
                  };
                  { name = "Alarming";
                    impl = "_build/default/src/cfa/analyzers/alarming.ml";
                    intf = "_build/default/src/cfa/analyzers/alarming.mli";
                    cmt =
                      "_build/default/src/cfa/analyzers/.Analyzers.objs/byte/analyzers__Alarming.cmt";
                    cmti =
                      "_build/default/src/cfa/analyzers/.Analyzers.objs/byte/analyzers__Alarming.cmti";
                    impl_deps = S.empty;
                    intf_deps = S.empty
                  };
                  { name = "Analyzers";
                    impl = "_build/default/src/cfa/analyzers/analyzers.ml-gen";
                    intf = "";
                    cmt =
                      "_build/default/src/cfa/analyzers/.Analyzers.objs/byte/analyzers.cmt";
                    cmti = "";
                    impl_deps = S.empty;
                    intf_deps = S.empty
                  }
                ];
              include_dirs =
                [ "_build/default/src/cfa/analyzers/.Analyzers.objs/byte" ]
            };
          Lib
            { name = "AbstractDomains";
              uid = "780b363f93badaa2c9e3caceb5f46835";
              local = true;
              requires = S.of_list [ "52603f9930efcca61c8e7216acb48efa" ];
              source_dir = "_build/default/src/cfa/abstract_domains";
              modules =
                [ { name = "TwoPoints";
                    impl =
                      "_build/default/src/cfa/abstract_domains/twoPoints.ml";
                    intf =
                      "_build/default/src/cfa/abstract_domains/twoPoints.mli";
                    cmt =
                      "_build/default/src/cfa/abstract_domains/.AbstractDomains.objs/byte/abstractDomains__TwoPoints.cmt";
                    cmti =
                      "_build/default/src/cfa/abstract_domains/.AbstractDomains.objs/byte/abstractDomains__TwoPoints.cmti";
                    impl_deps = S.empty;
                    intf_deps = S.empty
                  };
                  { name = "Sigs";
                    impl = "_build/default/src/cfa/abstract_domains/sigs.ml";
                    intf = "";
                    cmt =
                      "_build/default/src/cfa/abstract_domains/.AbstractDomains.objs/byte/abstractDomains__Sigs.cmt";
                    cmti = "";
                    impl_deps = S.empty;
                    intf_deps = S.empty
                  };
                  { name = "Signs";
                    impl = "_build/default/src/cfa/abstract_domains/signs.ml";
                    intf = "_build/default/src/cfa/abstract_domains/signs.mli";
                    cmt =
                      "_build/default/src/cfa/abstract_domains/.AbstractDomains.objs/byte/abstractDomains__Signs.cmt";
                    cmti =
                      "_build/default/src/cfa/abstract_domains/.AbstractDomains.objs/byte/abstractDomains__Signs.cmti";
                    impl_deps = S.empty;
                    intf_deps = S.of_list [ "Sigs" ]
                  };
                  { name = "Intervals";
                    impl =
                      "_build/default/src/cfa/abstract_domains/intervals.ml";
                    intf =
                      "_build/default/src/cfa/abstract_domains/intervals.mli";
                    cmt =
                      "_build/default/src/cfa/abstract_domains/.AbstractDomains.objs/byte/abstractDomains__Intervals.cmt";
                    cmti =
                      "_build/default/src/cfa/abstract_domains/.AbstractDomains.objs/byte/abstractDomains__Intervals.cmti";
                    impl_deps = S.empty;
                    intf_deps = S.of_list [ "Sigs" ]
                  };
                  { name = "FlatInts";
                    impl = "_build/default/src/cfa/abstract_domains/flatInts.ml";
                    intf =
                      "_build/default/src/cfa/abstract_domains/flatInts.mli";
                    cmt =
                      "_build/default/src/cfa/abstract_domains/.AbstractDomains.objs/byte/abstractDomains__FlatInts.cmt";
                    cmti =
                      "_build/default/src/cfa/abstract_domains/.AbstractDomains.objs/byte/abstractDomains__FlatInts.cmti";
                    impl_deps = S.empty;
                    intf_deps = S.of_list [ "Sigs" ]
                  };
                  { name = "Bools";
                    impl = "_build/default/src/cfa/abstract_domains/bools.ml";
                    intf = "_build/default/src/cfa/abstract_domains/bools.mli";
                    cmt =
                      "_build/default/src/cfa/abstract_domains/.AbstractDomains.objs/byte/abstractDomains__Bools.cmt";
                    cmti =
                      "_build/default/src/cfa/abstract_domains/.AbstractDomains.objs/byte/abstractDomains__Bools.cmti";
                    impl_deps = S.empty;
                    intf_deps = S.of_list [ "Sigs" ]
                  };
                  { name = "AbstractDomains";
                    impl =
                      "_build/default/src/cfa/abstract_domains/abstractDomains.ml-gen";
                    intf = "";
                    cmt =
                      "_build/default/src/cfa/abstract_domains/.AbstractDomains.objs/byte/abstractDomains.cmt";
                    cmti = "";
                    impl_deps = S.empty;
                    intf_deps = S.empty
                  }
                ];
              include_dirs =
                [ "_build/default/src/cfa/abstract_domains/.AbstractDomains.objs/byte"
                ]
            };
          Exe
            { names = [ "generate_dune" ];
              requires = S.of_list [ "78de867ef1c43a82d19e5ad5c40b808c" ];
              modules =
                [ { name = "Heintze_mcallester_generator";
                    impl =
                      "_build/default/src/simpl/examples/heintze_mcallester_generator.ml";
                    intf = "";
                    cmt =
                      "_build/default/src/simpl/examples/.generate_dune.eobjs/byte/dune__exe__Heintze_mcallester_generator.cmt";
                    cmti = "";
                    impl_deps = S.empty;
                    intf_deps = S.empty
                  };
                  { name = "Generate_dune";
                    impl = "_build/default/src/simpl/examples/generate_dune.ml";
                    intf = "";
                    cmt =
                      "_build/default/src/simpl/examples/.generate_dune.eobjs/byte/dune__exe__Generate_dune.cmt";
                    cmti = "";
                    impl_deps = S.empty;
                    intf_deps = S.empty
                  };
                  { name = "Dune__exe";
                    impl =
                      "_build/default/src/simpl/examples/.generate_dune.eobjs/dune__exe.ml-gen";
                    intf = "";
                    cmt =
                      "_build/default/src/simpl/examples/.generate_dune.eobjs/byte/dune__exe.cmt";
                    cmti = "";
                    impl_deps = S.empty;
                    intf_deps = S.empty
                  }
                ];
              include_dirs =
                [ "_build/default/src/simpl/examples/.generate_dune.eobjs/byte"
                ]
            };
          Exe
            { names = [ "stableRels" ];
              requires =
                S.of_list
                  [ "9fe03ca268f7a71719c95f2542c21e8f";
                    "911c556d3b0ee62de3a0f35c494f5d49";
                    "498b5dbf60a477f5d2322629ef7bb03f";
                    "45c1801133a53c9543a1b42200314e05";
                    "339a601b8fe15c893c69ea594e7485b4";
                    "267ad208f39d68c3fe98b47f54e130cd"
                  ];
              modules =
                [ { name = "StableRels";
                    impl = "_build/default/src/simpl/demo/stableRels.ml";
                    intf = "";
                    cmt =
                      "_build/default/src/simpl/demo/.stableRels.eobjs/byte/dune__exe__StableRels.cmt";
                    cmti = "";
                    impl_deps = S.of_list [ "Examples" ];
                    intf_deps = S.empty
                  };
                  { name = "Examples";
                    impl = "_build/default/src/simpl/demo/examples.ml";
                    intf = "";
                    cmt =
                      "_build/default/src/simpl/demo/.stableRels.eobjs/byte/dune__exe__Examples.cmt";
                    cmti = "";
                    impl_deps = S.empty;
                    intf_deps = S.empty
                  };
                  { name = "Dune__exe";
                    impl =
                      "_build/default/src/simpl/demo/.stableRels.eobjs/dune__exe.ml-gen";
                    intf = "";
                    cmt =
                      "_build/default/src/simpl/demo/.stableRels.eobjs/byte/dune__exe.cmt";
                    cmti = "";
                    impl_deps = S.empty;
                    intf_deps = S.empty
                  }
                ];
              include_dirs =
                [ "_build/default/src/simpl/demo/.stableRels.eobjs/byte" ]
            };
          Exe
            { names = [ "scrape" ];
              requires = S.of_list [ "9f3c74fef7e4be5cad139b33c5b47f08" ];
              modules =
                [ { name = "Scrape";
                    impl = "_build/default/src/simpl/demo/scrape.ml";
                    intf = "";
                    cmt =
                      "_build/default/src/simpl/demo/.scrape.eobjs/byte/dune__exe__Scrape.cmt";
                    cmti = "";
                    impl_deps = S.empty;
                    intf_deps = S.empty
                  }
                ];
              include_dirs =
                [ "_build/default/src/simpl/demo/.scrape.eobjs/byte" ]
            };
          Exe
            { names = [ "simpl" ];
              requires =
                S.of_list
                  [ "c480a7c584d174c22d86dbdb79515d7d";
                    "339a601b8fe15c893c69ea594e7485b4"
                  ];
              modules =
                [ { name = "Simpl";
                    impl = "_build/default/src/simpl/cli/simpl.ml";
                    intf = "_build/default/src/simpl/cli/simpl.mli";
                    cmt =
                      "_build/default/src/simpl/cli/.simpl.eobjs/byte/dune__exe__Simpl.cmt";
                    cmti =
                      "_build/default/src/simpl/cli/.simpl.eobjs/byte/dune__exe__Simpl.cmti";
                    impl_deps = S.empty;
                    intf_deps = S.empty
                  }
                ];
              include_dirs =
                [ "_build/default/src/simpl/cli/.simpl.eobjs/byte" ]
            };
          Exe
            { names = [ "generate_bench" ];
              requires =
                S.of_list
                  [ "ae2633fbba7cfd1fe90fdf13d1a6b266";
                    "45c1801133a53c9543a1b42200314e05"
                  ];
              modules =
                [ { name = "Generate_bench";
                    impl = "_build/default/src/cfa/examples/generate_bench.ml";
                    intf = "";
                    cmt =
                      "_build/default/src/cfa/examples/.generate_bench.eobjs/byte/dune__exe__Generate_bench.cmt";
                    cmti = "";
                    impl_deps = S.empty;
                    intf_deps = S.empty
                  }
                ];
              include_dirs =
                [ "_build/default/src/cfa/examples/.generate_bench.eobjs/byte" ]
            };
          Exe
            { names = [ "generate_dune" ];
              requires = S.of_list [ "78de867ef1c43a82d19e5ad5c40b808c" ];
              modules =
                [ { name = "Generate_dune";
                    impl = "_build/default/src/cfa/examples/generate_dune.ml";
                    intf = "";
                    cmt =
                      "_build/default/src/cfa/examples/.generate_dune.eobjs/byte/dune__exe__Generate_dune.cmt";
                    cmti = "";
                    impl_deps = S.empty;
                    intf_deps = S.empty
                  }
                ];
              include_dirs =
                [ "_build/default/src/cfa/examples/.generate_dune.eobjs/byte" ]
            };
          Exe
            { names = [ "cfa" ];
              requires =
                S.of_list
                  [ "ae2633fbba7cfd1fe90fdf13d1a6b266";
                    "9fe03ca268f7a71719c95f2542c21e8f";
                    "911c556d3b0ee62de3a0f35c494f5d49";
                    "498b5dbf60a477f5d2322629ef7bb03f";
                    "45c1801133a53c9543a1b42200314e05";
                    "267ad208f39d68c3fe98b47f54e130cd"
                  ];
              modules =
                [ { name = "Examples";
                    impl = "_build/default/src/cfa/demo/examples.ml";
                    intf = "";
                    cmt =
                      "_build/default/src/cfa/demo/.cfa.eobjs/byte/dune__exe__Examples.cmt";
                    cmti = "";
                    impl_deps = S.empty;
                    intf_deps = S.empty
                  };
                  { name = "Cfa";
                    impl = "_build/default/src/cfa/demo/cfa.ml";
                    intf = "_build/default/src/cfa/demo/cfa.mli";
                    cmt =
                      "_build/default/src/cfa/demo/.cfa.eobjs/byte/dune__exe__Cfa.cmt";
                    cmti =
                      "_build/default/src/cfa/demo/.cfa.eobjs/byte/dune__exe__Cfa.cmti";
                    impl_deps = S.of_list [ "Examples" ];
                    intf_deps = S.empty
                  };
                  { name = "Dune__exe";
                    impl =
                      "_build/default/src/cfa/demo/.cfa.eobjs/dune__exe.ml-gen";
                    intf = "";
                    cmt =
                      "_build/default/src/cfa/demo/.cfa.eobjs/byte/dune__exe.cmt";
                    cmti = "";
                    impl_deps = S.empty;
                    intf_deps = S.empty
                  }
                ];
              include_dirs = [ "_build/default/src/cfa/demo/.cfa.eobjs/byte" ]
            };
          Exe
            { names = [ "scrape" ];
              requires = S.of_list [ "9f3c74fef7e4be5cad139b33c5b47f08" ];
              modules =
                [ { name = "Scrape";
                    impl = "_build/default/src/cfa/demo/scrape.ml";
                    intf = "";
                    cmt =
                      "_build/default/src/cfa/demo/.scrape.eobjs/byte/dune__exe__Scrape.cmt";
                    cmti = "";
                    impl_deps = S.empty;
                    intf_deps = S.empty
                  }
                ];
              include_dirs =
                [ "_build/default/src/cfa/demo/.scrape.eobjs/byte" ]
            };
          Exe
            { names = [ "cfa" ];
              requires =
                S.of_list
                  [ "c480a7c584d174c22d86dbdb79515d7d";
                    "ae2633fbba7cfd1fe90fdf13d1a6b266"
                  ];
              modules =
                [ { name = "Cfa";
                    impl = "_build/default/src/cfa/cli/cfa.ml";
                    intf = "_build/default/src/cfa/cli/cfa.mli";
                    cmt =
                      "_build/default/src/cfa/cli/.cfa.eobjs/byte/dune__exe__Cfa.cmt";
                    cmti =
                      "_build/default/src/cfa/cli/.cfa.eobjs/byte/dune__exe__Cfa.cmti";
                    impl_deps = S.empty;
                    intf_deps = S.empty
                  }
                ];
              include_dirs = [ "_build/default/src/cfa/cli/.cfa.eobjs/byte" ]
            }
        ]
    }

let%test _ =
  test_file "csexp"
    { root = ".";
      context = "_build/default";
      compilables =
        [ Lib
            { name = "time_now";
              uid = "66c5927ddd01193cdcfac8ce97e58e63";
              local = false;
              requires =
                S.of_list
                  [ "92685e7ac0dd1fa9cd96be221032349e";
                    "73ad4e016c34da2f2d4a1cff930ac883";
                    "708bf5748829e3636236f5d8c610f430";
                    "475f353b2705e034b0287d7ffe9e5225";
                    "46774e2b7a404647f12956126bd28f95";
                    "449445be7a24ce51e119d57e9e255d3f";
                    "2c6f959289bddfd3b3ada8f64a3ca5d8"
                  ];
              source_dir = "/home/pierre/.opam/4.10.0/lib/time_now";
              modules = [];
              include_dirs = [ "/home/pierre/.opam/4.10.0/lib/time_now" ]
            };
          Lib
            { name = "stdlib-shims";
              uid = "249b2edaf3cc552a247667041bb5f015";
              local = false;
              requires = S.empty;
              source_dir = "/home/pierre/.opam/4.10.0/lib/stdlib-shims";
              modules = [];
              include_dirs = [ "/home/pierre/.opam/4.10.0/lib/stdlib-shims" ]
            };
          Lib
            { name = "sexplib0";
              uid = "449445be7a24ce51e119d57e9e255d3f";
              local = false;
              requires = S.empty;
              source_dir = "/home/pierre/.opam/4.10.0/lib/sexplib0";
              modules = [];
              include_dirs = [ "/home/pierre/.opam/4.10.0/lib/sexplib0" ]
            };
          Lib
            { name = "ppxlib.traverse_builtins";
              uid = "24f4eb12e3ff51b310dbf7443c6087be";
              local = false;
              requires = S.empty;
              source_dir =
                "/home/pierre/.opam/4.10.0/lib/ppxlib/traverse_builtins";
              modules = [];
              include_dirs =
                [ "/home/pierre/.opam/4.10.0/lib/ppxlib/traverse_builtins" ]
            };
          Lib
            { name = "ppxlib.stdppx";
              uid = "5ae836dcdead11d5c16815297c5a1ae6";
              local = false;
              requires =
                S.of_list
                  [ "449445be7a24ce51e119d57e9e255d3f";
                    "249b2edaf3cc552a247667041bb5f015"
                  ];
              source_dir = "/home/pierre/.opam/4.10.0/lib/ppxlib/stdppx";
              modules = [];
              include_dirs = [ "/home/pierre/.opam/4.10.0/lib/ppxlib/stdppx" ]
            };
          Lib
            { name = "ppxlib.print_diff";
              uid = "43b7cbe1f93f4f502ec614971027cff9";
              local = false;
              requires = S.empty;
              source_dir = "/home/pierre/.opam/4.10.0/lib/ppxlib/print_diff";
              modules = [];
              include_dirs =
                [ "/home/pierre/.opam/4.10.0/lib/ppxlib/print_diff" ]
            };
          Lib
            { name = "ppxlib.astlib";
              uid = "5014e215e204cf8da6c32644cda1b31e";
              local = false;
              requires =
                S.of_list
                  [ "c9367091ddd9a70d99fc22ede348f17c";
                    "1f2b5eb300ea716920494385a31bb5fb"
                  ];
              source_dir = "/home/pierre/.opam/4.10.0/lib/ppxlib/astlib";
              modules = [];
              include_dirs = [ "/home/pierre/.opam/4.10.0/lib/ppxlib/astlib" ]
            };
          Lib
            { name = "ppxlib.ast";
              uid = "ba85adfb1c97e7d7af3df35b16b2fc0d";
              local = false;
              requires =
                S.of_list
                  [ "5014e215e204cf8da6c32644cda1b31e";
                    "249b2edaf3cc552a247667041bb5f015"
                  ];
              source_dir = "/home/pierre/.opam/4.10.0/lib/ppxlib/ast";
              modules = [];
              include_dirs = [ "/home/pierre/.opam/4.10.0/lib/ppxlib/ast" ]
            };
          Lib
            { name = "ppxlib";
              uid = "2c61db8e94cb08e0fe642152aee8121a";
              local = false;
              requires =
                S.of_list
                  [ "e68a558facd1546b51c7abdbf6aed1cb";
                    "ba85adfb1c97e7d7af3df35b16b2fc0d";
                    "5ae836dcdead11d5c16815297c5a1ae6";
                    "5014e215e204cf8da6c32644cda1b31e";
                    "449445be7a24ce51e119d57e9e255d3f";
                    "43b7cbe1f93f4f502ec614971027cff9";
                    "24f4eb12e3ff51b310dbf7443c6087be";
                    "249b2edaf3cc552a247667041bb5f015";
                    "2363fd46dac995a1c79679dfa1a9881b"
                  ];
              source_dir = "/home/pierre/.opam/4.10.0/lib/ppxlib";
              modules = [];
              include_dirs = [ "/home/pierre/.opam/4.10.0/lib/ppxlib" ]
            };
          Lib
            { name = "ppx_sexp_conv.runtime-lib";
              uid = "475f353b2705e034b0287d7ffe9e5225";
              local = false;
              requires = S.of_list [ "449445be7a24ce51e119d57e9e255d3f" ];
              source_dir =
                "/home/pierre/.opam/4.10.0/lib/ppx_sexp_conv/runtime-lib";
              modules = [];
              include_dirs =
                [ "/home/pierre/.opam/4.10.0/lib/ppx_sexp_conv/runtime-lib" ]
            };
          Lib
            { name = "ppx_inline_test.runtime-lib";
              uid = "55b023c301c18e021a22384b996d66af";
              local = false;
              requires =
                S.of_list
                  [ "66c5927ddd01193cdcfac8ce97e58e63";
                    "46e75006466e7a020139d86575978cb3";
                    "46774e2b7a404647f12956126bd28f95"
                  ];
              source_dir =
                "/home/pierre/.opam/4.10.0/lib/ppx_inline_test/runtime-lib";
              modules = [];
              include_dirs =
                [ "/home/pierre/.opam/4.10.0/lib/ppx_inline_test/runtime-lib" ]
            };
          Lib
            { name = "ppx_inline_test.libname";
              uid = "08f2b871a278556c65a5e886ad6772ec";
              local = false;
              requires =
                S.of_list
                  [ "ba85adfb1c97e7d7af3df35b16b2fc0d";
                    "2c61db8e94cb08e0fe642152aee8121a"
                  ];
              source_dir =
                "/home/pierre/.opam/4.10.0/lib/ppx_inline_test/libname";
              modules = [];
              include_dirs =
                [ "/home/pierre/.opam/4.10.0/lib/ppx_inline_test/libname" ]
            };
          Lib
            { name = "ppx_inline_test.config";
              uid = "46e75006466e7a020139d86575978cb3";
              local = false;
              requires = S.empty;
              source_dir =
                "/home/pierre/.opam/4.10.0/lib/ppx_inline_test/config";
              modules = [];
              include_dirs =
                [ "/home/pierre/.opam/4.10.0/lib/ppx_inline_test/config" ]
            };
          Lib
            { name = "ppx_inline_test";
              uid = "e90d385812291a9fcde3d83613d76c4d";
              local = false;
              requires =
                S.of_list
                  [ "ba85adfb1c97e7d7af3df35b16b2fc0d";
                    "46774e2b7a404647f12956126bd28f95";
                    "2c61db8e94cb08e0fe642152aee8121a";
                    "08f2b871a278556c65a5e886ad6772ec"
                  ];
              source_dir = "/home/pierre/.opam/4.10.0/lib/ppx_inline_test";
              modules = [];
              include_dirs = [ "/home/pierre/.opam/4.10.0/lib/ppx_inline_test" ]
            };
          Lib
            { name = "ppx_here.runtime-lib";
              uid = "20ba2ea90fd3f2c1e62d9cb321b1a9eb";
              local = false;
              requires = S.empty;
              source_dir = "/home/pierre/.opam/4.10.0/lib/ppx_here/runtime-lib";
              modules = [];
              include_dirs =
                [ "/home/pierre/.opam/4.10.0/lib/ppx_here/runtime-lib" ]
            };
          Lib
            { name = "ppx_here.expander";
              uid = "38e359c1ff0967c18a9ebc9b981e5a9e";
              local = false;
              requires =
                S.of_list
                  [ "ba85adfb1c97e7d7af3df35b16b2fc0d";
                    "46774e2b7a404647f12956126bd28f95";
                    "2c61db8e94cb08e0fe642152aee8121a"
                  ];
              source_dir = "/home/pierre/.opam/4.10.0/lib/ppx_here/expander";
              modules = [];
              include_dirs =
                [ "/home/pierre/.opam/4.10.0/lib/ppx_here/expander" ]
            };
          Lib
            { name = "ppx_hash.runtime-lib";
              uid = "92685e7ac0dd1fa9cd96be221032349e";
              local = false;
              requires =
                S.of_list
                  [ "708bf5748829e3636236f5d8c610f430";
                    "475f353b2705e034b0287d7ffe9e5225";
                    "46774e2b7a404647f12956126bd28f95";
                    "449445be7a24ce51e119d57e9e255d3f"
                  ];
              source_dir = "/home/pierre/.opam/4.10.0/lib/ppx_hash/runtime-lib";
              modules = [];
              include_dirs =
                [ "/home/pierre/.opam/4.10.0/lib/ppx_hash/runtime-lib" ]
            };
          Lib
            { name = "ppx_expect.payload";
              uid = "7e5ca295bb33ccc8be62b7243d03ba47";
              local = false;
              requires =
                S.of_list
                  [ "ba85adfb1c97e7d7af3df35b16b2fc0d";
                    "5f5174092107521c0d4ecf21084d43c7";
                    "2c61db8e94cb08e0fe642152aee8121a"
                  ];
              source_dir = "/home/pierre/.opam/4.10.0/lib/ppx_expect/payload";
              modules = [];
              include_dirs =
                [ "/home/pierre/.opam/4.10.0/lib/ppx_expect/payload" ]
            };
          Lib
            { name = "ppx_expect.config_types";
              uid = "e98bd409a98e65033ef31f23d34c99c3";
              local = false;
              requires = S.empty;
              source_dir =
                "/home/pierre/.opam/4.10.0/lib/ppx_expect/config_types";
              modules = [];
              include_dirs =
                [ "/home/pierre/.opam/4.10.0/lib/ppx_expect/config_types" ]
            };
          Lib
            { name = "ppx_expect.config";
              uid = "690760db8a30f8e8f4021797e13ede81";
              local = false;
              requires = S.of_list [ "e98bd409a98e65033ef31f23d34c99c3" ];
              source_dir = "/home/pierre/.opam/4.10.0/lib/ppx_expect/config";
              modules = [];
              include_dirs =
                [ "/home/pierre/.opam/4.10.0/lib/ppx_expect/config" ]
            };
          Lib
            { name = "ppx_expect.common";
              uid = "5f5174092107521c0d4ecf21084d43c7";
              local = false;
              requires = S.of_list [ "46774e2b7a404647f12956126bd28f95" ];
              source_dir = "/home/pierre/.opam/4.10.0/lib/ppx_expect/common";
              modules = [];
              include_dirs =
                [ "/home/pierre/.opam/4.10.0/lib/ppx_expect/common" ]
            };
          Lib
            { name = "ppx_expect.collector";
              uid = "944581b51422e2148578565cb0425f31";
              local = false;
              requires =
                S.of_list
                  [ "e98bd409a98e65033ef31f23d34c99c3";
                    "5f5174092107521c0d4ecf21084d43c7";
                    "55b023c301c18e021a22384b996d66af"
                  ];
              source_dir = "/home/pierre/.opam/4.10.0/lib/ppx_expect/collector";
              modules = [];
              include_dirs =
                [ "/home/pierre/.opam/4.10.0/lib/ppx_expect/collector" ]
            };
          Lib
            { name = "ppx_expect";
              uid = "bd50825c4f12f6b9c392a28985698a62";
              local = false;
              requires =
                S.of_list
                  [ "e90d385812291a9fcde3d83613d76c4d";
                    "ba85adfb1c97e7d7af3df35b16b2fc0d";
                    "7e5ca295bb33ccc8be62b7243d03ba47";
                    "5f5174092107521c0d4ecf21084d43c7";
                    "46774e2b7a404647f12956126bd28f95";
                    "38e359c1ff0967c18a9ebc9b981e5a9e";
                    "2c61db8e94cb08e0fe642152aee8121a";
                    "08f2b871a278556c65a5e886ad6772ec"
                  ];
              source_dir = "/home/pierre/.opam/4.10.0/lib/ppx_expect";
              modules = [];
              include_dirs = [ "/home/pierre/.opam/4.10.0/lib/ppx_expect" ]
            };
          Lib
            { name = "ppx_enumerate.runtime-lib";
              uid = "2c6f959289bddfd3b3ada8f64a3ca5d8";
              local = false;
              requires = S.empty;
              source_dir =
                "/home/pierre/.opam/4.10.0/lib/ppx_enumerate/runtime-lib";
              modules = [];
              include_dirs =
                [ "/home/pierre/.opam/4.10.0/lib/ppx_enumerate/runtime-lib" ]
            };
          Lib
            { name = "ppx_derivers";
              uid = "e68a558facd1546b51c7abdbf6aed1cb";
              local = false;
              requires = S.empty;
              source_dir = "/home/pierre/.opam/4.10.0/lib/ppx_derivers";
              modules = [];
              include_dirs = [ "/home/pierre/.opam/4.10.0/lib/ppx_derivers" ]
            };
          Lib
            { name = "ppx_compare.runtime-lib";
              uid = "708bf5748829e3636236f5d8c610f430";
              local = false;
              requires = S.of_list [ "46774e2b7a404647f12956126bd28f95" ];
              source_dir =
                "/home/pierre/.opam/4.10.0/lib/ppx_compare/runtime-lib";
              modules = [];
              include_dirs =
                [ "/home/pierre/.opam/4.10.0/lib/ppx_compare/runtime-lib" ]
            };
          Lib
            { name = "ppx_bench.runtime-lib";
              uid = "0a5ac475dc06cf90fd25efabd674a231";
              local = false;
              requires = S.empty;
              source_dir = "/home/pierre/.opam/4.10.0/lib/ppx_bench/runtime-lib";
              modules = [];
              include_dirs =
                [ "/home/pierre/.opam/4.10.0/lib/ppx_bench/runtime-lib" ]
            };
          Lib
            { name = "ppx_bench";
              uid = "a364c7b5d882efea8330804d0f2a154c";
              local = false;
              requires =
                S.of_list
                  [ "ba85adfb1c97e7d7af3df35b16b2fc0d";
                    "2c61db8e94cb08e0fe642152aee8121a";
                    "08f2b871a278556c65a5e886ad6772ec"
                  ];
              source_dir = "/home/pierre/.opam/4.10.0/lib/ppx_bench";
              modules = [];
              include_dirs = [ "/home/pierre/.opam/4.10.0/lib/ppx_bench" ]
            };
          Lib
            { name = "ocaml-compiler-libs.shadow";
              uid = "2363fd46dac995a1c79679dfa1a9881b";
              local = false;
              requires = S.empty;
              source_dir =
                "/home/pierre/.opam/4.10.0/lib/ocaml-compiler-libs/shadow";
              modules = [];
              include_dirs =
                [ "/home/pierre/.opam/4.10.0/lib/ocaml-compiler-libs/shadow" ]
            };
          Lib
            { name = "ocaml-compiler-libs.common";
              uid = "1f2b5eb300ea716920494385a31bb5fb";
              local = false;
              requires = S.of_list [ "c9367091ddd9a70d99fc22ede348f17c" ];
              source_dir =
                "/home/pierre/.opam/4.10.0/lib/ocaml-compiler-libs/common";
              modules = [];
              include_dirs =
                [ "/home/pierre/.opam/4.10.0/lib/ocaml-compiler-libs/common" ]
            };
          Lib
            { name = "jane-street-headers";
              uid = "73ad4e016c34da2f2d4a1cff930ac883";
              local = false;
              requires = S.empty;
              source_dir = "/home/pierre/.opam/4.10.0/lib/jane-street-headers";
              modules = [];
              include_dirs =
                [ "/home/pierre/.opam/4.10.0/lib/jane-street-headers" ]
            };
          Lib
            { name = "csexp_tests";
              uid = "cae2600322b2b8a67c297c353294d54d";
              local = true;
              requires =
                S.of_list
                  [ "944581b51422e2148578565cb0425f31";
                    "690760db8a30f8e8f4021797e13ede81";
                    "55b023c301c18e021a22384b996d66af";
                    "46e75006466e7a020139d86575978cb3";
                    "2ac224c04fa61d226241d5394bcfc444";
                    "20ba2ea90fd3f2c1e62d9cb321b1a9eb"
                  ];
              source_dir = "_build/default/test";
              modules =
                [ { name = "Test";
                    impl = "_build/default/test/test.ml";
                    intf = "";
                    cmt =
                      "_build/default/test/.csexp_tests.objs/byte/csexp_tests__Test.cmt";
                    cmti = "";
                    impl_deps = S.empty;
                    intf_deps = S.empty
                  };
                  { name = "Csexp_tests";
                    impl = "_build/default/test/csexp_tests.ml-gen";
                    intf = "";
                    cmt =
                      "_build/default/test/.csexp_tests.objs/byte/csexp_tests.cmt";
                    cmti = "";
                    impl_deps = S.empty;
                    intf_deps = S.empty
                  }
                ];
              include_dirs = [ "_build/default/test/.csexp_tests.objs/byte" ]
            };
          Lib
            { name = "csexp_bench";
              uid = "c18dc1aff21f9ab14695aebc5e336ed9";
              local = true;
              requires =
                S.of_list
                  [ "2ac224c04fa61d226241d5394bcfc444";
                    "0a5ac475dc06cf90fd25efabd674a231"
                  ];
              source_dir = "_build/default/bench";
              modules =
                [ { name = "Csexp_bench";
                    impl = "_build/default/bench/csexp_bench.ml";
                    intf = "";
                    cmt =
                      "_build/default/bench/.csexp_bench.objs/byte/csexp_bench.cmt";
                    cmti = "";
                    impl_deps = S.empty;
                    intf_deps = S.empty
                  }
                ];
              include_dirs = [ "_build/default/bench/.csexp_bench.objs/byte" ]
            };
          Lib
            { name = "csexp";
              uid = "2ac224c04fa61d226241d5394bcfc444";
              local = true;
              requires = S.empty;
              source_dir = "_build/default/src";
              modules =
                [ { name = "Csexp";
                    impl = "_build/default/src/csexp.ml";
                    intf = "_build/default/src/csexp.mli";
                    cmt = "_build/default/src/.csexp.objs/byte/csexp.cmt";
                    cmti = "_build/default/src/.csexp.objs/byte/csexp.cmti";
                    impl_deps = S.empty;
                    intf_deps = S.empty
                  }
                ];
              include_dirs = [ "_build/default/src/.csexp.objs/byte" ]
            };
          Lib
            { name = "compiler-libs.common";
              uid = "c9367091ddd9a70d99fc22ede348f17c";
              local = false;
              requires = S.empty;
              source_dir = "/home/pierre/.opam/4.10.0/lib/ocaml/compiler-libs";
              modules = [];
              include_dirs =
                [ "/home/pierre/.opam/4.10.0/lib/ocaml/compiler-libs" ]
            };
          Lib
            { name = "base.shadow_stdlib";
              uid = "b91de1a8b6b882f5b4726d5b1f2ece6e";
              local = false;
              requires = S.of_list [ "ce20dcd0c4bae81524a96662594adde2" ];
              source_dir = "/home/pierre/.opam/4.10.0/lib/base/shadow_stdlib";
              modules = [];
              include_dirs =
                [ "/home/pierre/.opam/4.10.0/lib/base/shadow_stdlib" ]
            };
          Lib
            { name = "base.caml";
              uid = "ce20dcd0c4bae81524a96662594adde2";
              local = false;
              requires = S.empty;
              source_dir = "/home/pierre/.opam/4.10.0/lib/base/caml";
              modules = [];
              include_dirs = [ "/home/pierre/.opam/4.10.0/lib/base/caml" ]
            };
          Lib
            { name = "base.base_internalhash_types";
              uid = "3803213fe04d38cea3157f466b9a8747";
              local = false;
              requires = S.empty;
              source_dir =
                "/home/pierre/.opam/4.10.0/lib/base/base_internalhash_types";
              modules = [];
              include_dirs =
                [ "/home/pierre/.opam/4.10.0/lib/base/base_internalhash_types" ]
            };
          Lib
            { name = "base";
              uid = "46774e2b7a404647f12956126bd28f95";
              local = false;
              requires =
                S.of_list
                  [ "ce20dcd0c4bae81524a96662594adde2";
                    "b91de1a8b6b882f5b4726d5b1f2ece6e";
                    "449445be7a24ce51e119d57e9e255d3f";
                    "3803213fe04d38cea3157f466b9a8747"
                  ];
              source_dir = "/home/pierre/.opam/4.10.0/lib/base";
              modules = [];
              include_dirs = [ "/home/pierre/.opam/4.10.0/lib/base" ]
            }
        ]
    }

let%test _ =
  test_file "dmap"
    { root = ".";
      context = "_build/default";
      compilables =
        [ Lib
            { name = "dmap";
              uid = "3f8408925c6dbf918f7bafe5eae466ac";
              local = true;
              requires = S.of_list [];
              source_dir = "_build/default";
              modules =
                [ { name = "Dmap";
                    impl = "_build/default/dmap.ml";
                    intf = "_build/default/dmap.mli";
                    cmt = "_build/default/.dmap.objs/byte/dmap.cmt";
                    cmti = "_build/default/.dmap.objs/byte/dmap.cmti";
                    impl_deps = S.of_list [];
                    intf_deps = S.of_list []
                  }
                ];
              include_dirs = [ "_build/default/.dmap.objs/byte" ]
            }
        ]
    }

let%test _ =
  test_file "mfg"
    { root = ".";
      context = "_build/default";
      compilables =
        [ Lib
            { name = "unix";
              uid = "7c82c98fd32b5f392f3ad8c940ef1e9e";
              local = false;
              requires = S.of_list [];
              source_dir = "/home/pierre/.opam/4.10.0/lib/ocaml";
              modules = [];
              include_dirs = [ "/home/pierre/.opam/4.10.0/lib/ocaml" ]
            };
          Lib
            { name = "result";
              uid = "ca6cc2b0189077cce731d5327e658aa4";
              local = false;
              requires = S.of_list [];
              source_dir = "/home/pierre/.opam/4.10.0/lib/result";
              modules = [];
              include_dirs = [ "/home/pierre/.opam/4.10.0/lib/result" ]
            };
          Lib
            { name = "ppx_deriving.runtime";
              uid = "52603f9930efcca61c8e7216acb48efa";
              local = false;
              requires = S.of_list [ "ca6cc2b0189077cce731d5327e658aa4" ];
              source_dir = "/home/pierre/.opam/4.10.0/lib/ppx_deriving/runtime";
              modules = [];
              include_dirs =
                [ "/home/pierre/.opam/4.10.0/lib/ppx_deriving/runtime" ]
            };
          Lib
            { name = "menhirLib";
              uid = "c7b36f1326ae39dd97104fb95068e29f";
              local = false;
              requires = S.of_list [];
              source_dir = "/home/pierre/.opam/4.10.0/lib/menhirLib";
              modules = [];
              include_dirs = [ "/home/pierre/.opam/4.10.0/lib/menhirLib" ]
            };
          Lib
            { name = "functional_partitioning";
              uid = "653371f37513115a526a4d67d1a45724";
              local = true;
              requires = S.of_list [];
              source_dir = "_build/default/src/functional_partitioning";
              modules =
                [ { name = "Elementary";
                    impl =
                      "_build/default/src/functional_partitioning/elementary.ml";
                    intf =
                      "_build/default/src/functional_partitioning/elementary.mli";
                    cmt =
                      "_build/default/src/functional_partitioning/.functional_partitioning.objs/byte/functional_partitioning__Elementary.cmt";
                    cmti =
                      "_build/default/src/functional_partitioning/.functional_partitioning.objs/byte/functional_partitioning__Elementary.cmti";
                    impl_deps = S.of_list [];
                    intf_deps = S.of_list []
                  };
                  { name = "Basic";
                    impl = "_build/default/src/functional_partitioning/basic.ml";
                    intf =
                      "_build/default/src/functional_partitioning/basic.mli";
                    cmt =
                      "_build/default/src/functional_partitioning/.functional_partitioning.objs/byte/functional_partitioning__Basic.cmt";
                    cmti =
                      "_build/default/src/functional_partitioning/.functional_partitioning.objs/byte/functional_partitioning__Basic.cmti";
                    impl_deps = S.of_list [];
                    intf_deps = S.of_list []
                  };
                  { name = "Functional_partitioning";
                    impl =
                      "_build/default/src/functional_partitioning/functional_partitioning.ml-gen";
                    intf = "";
                    cmt =
                      "_build/default/src/functional_partitioning/.functional_partitioning.objs/byte/functional_partitioning.cmt";
                    cmti = "";
                    impl_deps = S.of_list [];
                    intf_deps = S.of_list []
                  }
                ];
              include_dirs =
                [ "_build/default/src/functional_partitioning/.functional_partitioning.objs/byte"
                ]
            };
          Lib
            { name = "cmdliner";
              uid = "c480a7c584d174c22d86dbdb79515d7d";
              local = false;
              requires = S.of_list [];
              source_dir = "/home/pierre/.opam/4.10.0/lib/cmdliner";
              modules = [];
              include_dirs = [ "/home/pierre/.opam/4.10.0/lib/cmdliner" ]
            };
          Lib
            { name = "DuneTestGen";
              uid = "03ade606c206fe893c3487759124ca1b";
              local = true;
              requires = S.of_list [];
              source_dir = "_build/default/lib/dune_test_gen";
              modules =
                [ { name = "DuneTestGen";
                    impl = "_build/default/lib/dune_test_gen/DuneTestGen.ml";
                    intf = "_build/default/lib/dune_test_gen/DuneTestGen.mli";
                    cmt =
                      "_build/default/lib/dune_test_gen/.DuneTestGen.objs/byte/duneTestGen.cmt";
                    cmti =
                      "_build/default/lib/dune_test_gen/.DuneTestGen.objs/byte/duneTestGen.cmti";
                    impl_deps = S.of_list [];
                    intf_deps = S.of_list []
                  }
                ];
              include_dirs =
                [ "_build/default/lib/dune_test_gen/.DuneTestGen.objs/byte" ]
            };
          Exe
            { names = [ "mfg" ];
              requires =
                S.of_list
                  [ "52603f9930efcca61c8e7216acb48efa";
                    "653371f37513115a526a4d67d1a45724";
                    "7c82c98fd32b5f392f3ad8c940ef1e9e";
                    "c480a7c584d174c22d86dbdb79515d7d";
                    "c7b36f1326ae39dd97104fb95068e29f"
                  ];
              modules =
                [ { name = "Sigs";
                    impl = "_build/default/src/sigs.ml";
                    intf = "";
                    cmt =
                      "_build/default/src/.mfg.eobjs/byte/dune__exe__Sigs.cmt";
                    cmti = "";
                    impl_deps = S.of_list [];
                    intf_deps = S.of_list []
                  };
                  { name = "Parser";
                    impl = "_build/default/src/parser.ml";
                    intf = "_build/default/src/parser.mli";
                    cmt =
                      "_build/default/src/.mfg.eobjs/byte/dune__exe__Parser.cmt";
                    cmti =
                      "_build/default/src/.mfg.eobjs/byte/dune__exe__Parser.cmti";
                    impl_deps = S.of_list [ "Ast" ];
                    intf_deps = S.of_list [ "Ast" ]
                  };
                  { name = "Mfg";
                    impl = "_build/default/src/mfg.ml";
                    intf = "_build/default/src/mfg.mli";
                    cmt =
                      "_build/default/src/.mfg.eobjs/byte/dune__exe__Mfg.cmt";
                    cmti =
                      "_build/default/src/.mfg.eobjs/byte/dune__exe__Mfg.cmti";
                    impl_deps =
                      S.of_list
                        [ "Ast";
                          "Fo2Instances";
                          "FoCommon";
                          "FoInstances";
                          "Ho2Instances";
                          "HoCommon";
                          "HoInstances";
                          "Lexer";
                          "MapPP";
                          "Parser"
                        ];
                    intf_deps = S.of_list []
                  };
                  { name = "MapPP";
                    impl = "_build/default/src/MapPP.ml";
                    intf = "_build/default/src/MapPP.mli";
                    cmt =
                      "_build/default/src/.mfg.eobjs/byte/dune__exe__MapPP.cmt";
                    cmti =
                      "_build/default/src/.mfg.eobjs/byte/dune__exe__MapPP.cmti";
                    impl_deps = S.of_list [];
                    intf_deps = S.of_list []
                  };
                  { name = "Lexer";
                    impl = "_build/default/src/lexer.ml";
                    intf = "";
                    cmt =
                      "_build/default/src/.mfg.eobjs/byte/dune__exe__Lexer.cmt";
                    cmti = "";
                    impl_deps = S.of_list [ "Parser" ];
                    intf_deps = S.of_list []
                  };
                  { name = "IntervalDomain";
                    impl = "_build/default/src/IntervalDomain.ml";
                    intf = "_build/default/src/IntervalDomain.mli";
                    cmt =
                      "_build/default/src/.mfg.eobjs/byte/dune__exe__IntervalDomain.cmt";
                    cmti =
                      "_build/default/src/.mfg.eobjs/byte/dune__exe__IntervalDomain.cmti";
                    impl_deps = S.of_list [];
                    intf_deps = S.of_list []
                  };
                  { name = "Homfg2";
                    impl = "_build/default/src/homfg2.ml";
                    intf = "_build/default/src/homfg2.mli";
                    cmt =
                      "_build/default/src/.mfg.eobjs/byte/dune__exe__Homfg2.cmt";
                    cmti =
                      "_build/default/src/.mfg.eobjs/byte/dune__exe__Homfg2.cmti";
                    impl_deps = S.of_list [ "Common"; "HoCommon"; "Sigs" ];
                    intf_deps = S.of_list [ "Common"; "HoCommon"; "Sigs" ]
                  };
                  { name = "Homfg";
                    impl = "_build/default/src/homfg.ml";
                    intf = "_build/default/src/homfg.mli";
                    cmt =
                      "_build/default/src/.mfg.eobjs/byte/dune__exe__Homfg.cmt";
                    cmti =
                      "_build/default/src/.mfg.eobjs/byte/dune__exe__Homfg.cmti";
                    impl_deps =
                      S.of_list [ "Common"; "ExtList"; "HoCommon"; "Sigs" ];
                    intf_deps = S.of_list [ "Common"; "HoCommon"; "Sigs" ]
                  };
                  { name = "HoInstances";
                    impl = "_build/default/src/HoInstances.ml";
                    intf = "_build/default/src/HoInstances.mli";
                    cmt =
                      "_build/default/src/.mfg.eobjs/byte/dune__exe__HoInstances.cmt";
                    cmti =
                      "_build/default/src/.mfg.eobjs/byte/dune__exe__HoInstances.cmti";
                    impl_deps =
                      S.of_list
                        [ "Common"; "ExtList"; "HoCommon"; "Homfg"; "Sigs" ];
                    intf_deps = S.of_list [ "Common"; "HoCommon"; "Sigs" ]
                  };
                  { name = "HoCommon";
                    impl = "_build/default/src/hoCommon.ml";
                    intf = "_build/default/src/hoCommon.mli";
                    cmt =
                      "_build/default/src/.mfg.eobjs/byte/dune__exe__HoCommon.cmt";
                    cmti =
                      "_build/default/src/.mfg.eobjs/byte/dune__exe__HoCommon.cmti";
                    impl_deps =
                      S.of_list
                        [ "Common";
                          "ExtList";
                          "IntervalDomain";
                          "MapPP";
                          "Sigs"
                        ];
                    intf_deps = S.of_list [ "Common"; "Sigs" ]
                  };
                  { name = "Ho2Instances";
                    impl = "_build/default/src/Ho2Instances.ml";
                    intf = "_build/default/src/Ho2Instances.mli";
                    cmt =
                      "_build/default/src/.mfg.eobjs/byte/dune__exe__Ho2Instances.cmt";
                    cmti =
                      "_build/default/src/.mfg.eobjs/byte/dune__exe__Ho2Instances.cmti";
                    impl_deps =
                      S.of_list
                        [ "Common"; "ExtList"; "HoCommon"; "Homfg2"; "Sigs" ];
                    intf_deps = S.of_list [ "Common"; "HoCommon"; "Sigs" ]
                  };
                  { name = "Fomfg2";
                    impl = "_build/default/src/fomfg2.ml";
                    intf = "_build/default/src/fomfg2.mli";
                    cmt =
                      "_build/default/src/.mfg.eobjs/byte/dune__exe__Fomfg2.cmt";
                    cmti =
                      "_build/default/src/.mfg.eobjs/byte/dune__exe__Fomfg2.cmti";
                    impl_deps = S.of_list [ "Common"; "FoCommon"; "Sigs" ];
                    intf_deps = S.of_list [ "Common"; "FoCommon"; "Sigs" ]
                  };
                  { name = "Fomfg";
                    impl = "_build/default/src/fomfg.ml";
                    intf = "_build/default/src/fomfg.mli";
                    cmt =
                      "_build/default/src/.mfg.eobjs/byte/dune__exe__Fomfg.cmt";
                    cmti =
                      "_build/default/src/.mfg.eobjs/byte/dune__exe__Fomfg.cmti";
                    impl_deps = S.of_list [ "Common"; "FoCommon"; "Sigs" ];
                    intf_deps = S.of_list [ "Common"; "FoCommon"; "Sigs" ]
                  };
                  { name = "FoInstances";
                    impl = "_build/default/src/FoInstances.ml";
                    intf = "_build/default/src/FoInstances.mli";
                    cmt =
                      "_build/default/src/.mfg.eobjs/byte/dune__exe__FoInstances.cmt";
                    cmti =
                      "_build/default/src/.mfg.eobjs/byte/dune__exe__FoInstances.cmti";
                    impl_deps =
                      S.of_list [ "Common"; "FoCommon"; "Fomfg"; "Sigs" ];
                    intf_deps = S.of_list [ "Common"; "FoCommon"; "Sigs" ]
                  };
                  { name = "FoCommon";
                    impl = "_build/default/src/FoCommon.ml";
                    intf = "_build/default/src/FoCommon.mli";
                    cmt =
                      "_build/default/src/.mfg.eobjs/byte/dune__exe__FoCommon.cmt";
                    cmti =
                      "_build/default/src/.mfg.eobjs/byte/dune__exe__FoCommon.cmti";
                    impl_deps =
                      S.of_list [ "Common"; "IntervalDomain"; "MapPP"; "Sigs" ];
                    intf_deps = S.of_list [ "Common"; "Sigs" ]
                  };
                  { name = "Fo2Instances";
                    impl = "_build/default/src/Fo2Instances.ml";
                    intf = "_build/default/src/Fo2Instances.mli";
                    cmt =
                      "_build/default/src/.mfg.eobjs/byte/dune__exe__Fo2Instances.cmt";
                    cmti =
                      "_build/default/src/.mfg.eobjs/byte/dune__exe__Fo2Instances.cmti";
                    impl_deps =
                      S.of_list [ "Common"; "FoCommon"; "Fomfg2"; "Sigs" ];
                    intf_deps = S.of_list [ "Common"; "FoCommon"; "Sigs" ]
                  };
                  { name = "ExtList";
                    impl = "_build/default/src/ExtList.ml";
                    intf = "_build/default/src/ExtList.mli";
                    cmt =
                      "_build/default/src/.mfg.eobjs/byte/dune__exe__ExtList.cmt";
                    cmti =
                      "_build/default/src/.mfg.eobjs/byte/dune__exe__ExtList.cmti";
                    impl_deps = S.of_list [];
                    intf_deps = S.of_list []
                  };
                  { name = "Common";
                    impl = "_build/default/src/common.ml";
                    intf = "_build/default/src/common.mli";
                    cmt =
                      "_build/default/src/.mfg.eobjs/byte/dune__exe__Common.cmt";
                    cmti =
                      "_build/default/src/.mfg.eobjs/byte/dune__exe__Common.cmti";
                    impl_deps = S.of_list [ "Sigs" ];
                    intf_deps = S.of_list [ "Sigs" ]
                  };
                  { name = "Ast";
                    impl = "_build/default/src/ast.ml";
                    intf = "";
                    cmt =
                      "_build/default/src/.mfg.eobjs/byte/dune__exe__Ast.cmt";
                    cmti = "";
                    impl_deps = S.of_list [ "Common"; "FoCommon"; "HoCommon" ];
                    intf_deps = S.of_list []
                  };
                  { name = "Dune__exe";
                    impl = "_build/default/src/.mfg.eobjs/dune__exe.ml-gen";
                    intf = "";
                    cmt = "_build/default/src/.mfg.eobjs/byte/dune__exe.cmt";
                    cmti = "";
                    impl_deps = S.of_list [];
                    intf_deps = S.of_list []
                  }
                ];
              include_dirs = [ "_build/default/src/.mfg.eobjs/byte" ]
            };
          Exe
            { names = [ "generate_dune" ];
              requires = S.of_list [ "03ade606c206fe893c3487759124ca1b" ];
              modules =
                [ { name = "Generate_dune";
                    impl = "_build/default/examples/ho/generate_dune.ml";
                    intf = "";
                    cmt =
                      "_build/default/examples/ho/.generate_dune.eobjs/byte/dune__exe__Generate_dune.cmt";
                    cmti = "";
                    impl_deps = S.of_list [];
                    intf_deps = S.of_list []
                  }
                ];
              include_dirs =
                [ "_build/default/examples/ho/.generate_dune.eobjs/byte" ]
            };
          Exe
            { names = [ "generate_dune" ];
              requires = S.of_list [ "03ade606c206fe893c3487759124ca1b" ];
              modules =
                [ { name = "Generate_dune";
                    impl = "_build/default/examples/fo/generate_dune.ml";
                    intf = "";
                    cmt =
                      "_build/default/examples/fo/.generate_dune.eobjs/byte/dune__exe__Generate_dune.cmt";
                    cmti = "";
                    impl_deps = S.of_list [];
                    intf_deps = S.of_list []
                  }
                ];
              include_dirs =
                [ "_build/default/examples/fo/.generate_dune.eobjs/byte" ]
            }
        ]
    }
