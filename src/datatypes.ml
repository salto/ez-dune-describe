(** Copyright © Inria 2022

    @author Pierre Lermusiaux <pierre.lermusiaux@inria.fr>
    @author Benoît Montagu <benoit.montagu@inria.fr> *)

open Format
module S = Set.Make (String)
module M = Map.Make (String)

type set = S.t

(* Formatters *)
module type FORMATTABLE = sig
  type t

  val fprint : Format.formatter -> t -> unit
end

module type DEPENDABLE = sig
  type t

  val get_id : t -> string
  val get_deps : t -> set
end

module type FOLDABLE = sig
  module Dep : DEPENDABLE

  type t

  val get_fold_list : t -> Dep.t list
end

(* Topological iterators *)
module type FOLD = sig
  type t
  type d

  exception CircularDependency

  val fold : (d -> 'a -> 'a) -> t -> 'a -> 'a
  val iter : (d -> unit) -> t -> unit

  (*
   * val fold_inv : (d -> 'a -> 'a) -> t -> 'a -> 'a
   * val iter_inv : (d -> unit) -> t -> unit
   *)
end

(* Datatypes *)
module Module : sig
  type t =
    { name : string;
      impl : string;
      intf : string;
      cmt : string;
      cmti : string;
      impl_deps : set;
      intf_deps : set
    }

  include FORMATTABLE with type t := t
  include DEPENDABLE with type t := t
end = struct
  type t =
    { name : string;
      impl : string;
      intf : string;
      cmt : string;
      cmti : string;
      impl_deps : set;
      intf_deps : set
    }

  let get_id m = m.name
  let get_deps m = m.impl_deps

  (* Formatters *)
  let format_deps ppf deps =
    if S.is_empty deps then fprintf ppf "deps: [ ]"
    else
      let folder d wrapped f = wrapped (fun ppf -> fprintf ppf "%s;@ %t" d f) in
      let wrapper = fprintf ppf "deps: [@[<v>%t@]]" in
      S.fold folder deps wrapper (fun _ -> ())

  let fprint ppf m =
    let format_impl ppf = fprintf ppf "impl: %s" m.impl in
    let format_intf ppf = fprintf ppf "intf: %s" m.intf in
    let format_cmt ppf = fprintf ppf "cmt: %s" m.cmt in
    let format_cmti ppf = fprintf ppf "cmti: %s" m.cmti in
    fprintf ppf "{@[<v 1> Module %s@ %t;@ %t;@ %t;@ %t;@ impl_%a;@ intf_%a@ @]}"
      m.name format_impl format_intf format_cmt format_cmti format_deps
      m.impl_deps format_deps m.intf_deps
end

module Compilable : sig
  type t =
    | Exe of
        { names : string list;
          requires : set;
          modules : Module.t list;
          include_dirs : string list
        }
    | Lib of
        { name : string;
          uid : string;
          local : bool;
          requires : set;
          source_dir : string;
          modules : Module.t list;
          include_dirs : string list
        }

  include FORMATTABLE with type t := t
  include DEPENDABLE with type t := t
  include FOLDABLE with type t := t and module Dep = Module
end = struct
  module Dep = Module

  type t =
    | Exe of
        { names : string list;
          requires : set;
          modules : Module.t list;
          include_dirs : string list
        }
    | Lib of
        { name : string;
          uid : string;
          local : bool;
          requires : set;
          source_dir : string;
          modules : Module.t list;
          include_dirs : string list
        }

  let get_id = function
    | Exe _ -> assert false
    (* Executables do not have a unique identifier and will need to be
     * handled separately *)
    | Lib l -> l.uid

  let get_deps = function Exe e -> e.requires | Lib l -> l.requires
  let get_fold_list = function Exe e -> e.modules | Lib l -> l.modules

  (* Formatters *)
  let format_modules ppf = function
    | [] -> fprintf ppf "modules: [ ]"
    | ml ->
        let pp_sep ppf () = fprintf ppf ";@ " in
        fprintf ppf "modules: [@;@[<v>%a@]]"
          (pp_print_list ~pp_sep Module.fprint)
          ml

  let format_requires ppf deps =
    if S.is_empty deps then fprintf ppf "requires: [ ]"
    else
      let folder d wrapped f = wrapped (fun ppf -> fprintf ppf "%s;@ %t" d f) in
      let wrapper = fprintf ppf "requires: [@[<v>%t@]]" in
      S.fold folder deps wrapper (fun _ -> ())

  let format_include_dirs =
    let pp_sep ppf () = fprintf ppf ", " in
    pp_print_list ~pp_sep pp_print_string

  let fprint ppf = function
    | Exe e ->
        let pp_sep ppf () = fprintf ppf ", " in
        let format_names = pp_print_list ~pp_sep pp_print_string in
        fprintf ppf "{@[<v 1> Executables %a@ %a;@ %a;@ %a@ @]}" format_names
          e.names format_requires e.requires format_modules e.modules
          format_include_dirs e.include_dirs
    | Lib l ->
        let format_uid ppf = fprintf ppf "uid: %s" l.uid in
        let format_local ppf = fprintf ppf "local: %b" l.local in
        let format_source_dir ppf = fprintf ppf "source_dir: %s" l.source_dir in
        fprintf ppf "{@[<v 1> Library %s@ %t;@ %t;@ %a;@ %t;@ %a;@ %a@ @]}"
          l.name format_uid format_local format_requires l.requires
          format_source_dir format_modules l.modules format_include_dirs
          l.include_dirs
end

(* Topological Iterators *)
module Make (X : FOLDABLE) : FOLD with type t := X.t and type d := X.Dep.t =
struct
  exception CircularDependency

  (** [get_root f] returns a list of dependables with no dependency *)
  let get_root f =
    let is_root d = S.is_empty (X.Dep.get_deps d) in
    List.filter is_root (X.get_fold_list f)

  (** [get_dep_map f] builds a map of tuple (deg, inv_deps) for each dependable
      of the foldable, where deg is the number of dependencies, and inv_deps a
      list of depending dependables *)
  let get_dep_map f =
    let acc_ideps d = function
      | Some (deg, ideps) -> Some (deg, d :: ideps)
      | None -> Some (0, [ d ])
    in
    let insert deps = function
      | Some (_, ideps) -> Some (S.cardinal deps, ideps)
      | None -> Some (S.cardinal deps, [])
    in
    let update_deps d dep m = M.update dep (acc_ideps d) m in
    let build_dep_map m d =
      let id = X.Dep.get_id d in
      let deps = X.Dep.get_deps d in
      S.fold (update_deps d) deps (M.update id (insert deps) m)
    in
    List.fold_left build_dep_map M.empty (X.get_fold_list f)

  let fold g f x =
    let dep_map = get_dep_map f in
    let card = M.cardinal dep_map in
    let queue = Queue.create () in
    List.iter (fun d -> Queue.push d queue) (get_root f);
    let feed_queue m d =
      let updater = function
        | Some (1, ideps) ->
            Queue.push d queue;
            (* add dependables with no unresolved dependency to the queue *)
            Some (0, ideps)
        | Some (deg, ideps) -> Some (deg - 1, ideps)
        (* decrease the number of unresolved dependency of the dependable *)
        | None -> assert false
      in
      M.update (X.Dep.get_id d) updater m
    in
    let rec folder m n y =
      if Queue.is_empty queue then
        if n = card then y
        else raise CircularDependency
          (* if the queue is empty and some dependables have not been added to
           * the queue, there is (most likely) a circular dependency *)
      else
        (* computes the fold function with the first dependable of the queue,
         * updates the dependency map by decreasing the number of unresolved
         * dependency of its dependants, feeding the queue with dependants
         * having no unresolved dependency and increasing folded dependables
         * counter *)
        let d = Queue.pop queue in
        let _, ideps = M.find (X.Dep.get_id d) m in
        let dm = List.fold_left feed_queue m ideps in
        folder dm (n + 1) (g d y)
    in
    folder dep_map 0 x

  let iter g f =
    let thunk x () = g x in
    fold thunk f ()

  (*
   * let fold_inv g f x =
   *
   * let iter_inv g f =
   *)
end

module Workspace : sig
  type t =
    { root : string;
      context : string;
      compilables : Compilable.t list
    }

  include FORMATTABLE with type t := t
  include FOLDABLE with type t := t and module Dep = Compilable
  include FOLD with type t := t and type d := Module.t

  val foldCompilables : (Compilable.t -> 'a -> 'a) -> t -> 'a -> 'a

  exception UnresolvedDependency of Compilable.t
end = struct
  (* Inner FOLDABLE module in order to use the Make functor *)
  module WorkspaceF = struct
    module Dep = Compilable

    type t =
      { root : string;
        context : string;
        compilables : Compilable.t list
      }

    (* Executables do not have a unique identifier and will need to be
     * handled separately *)
    let get_fold_list w =
      let open Compilable in
      let is_lib = function Exe _ -> false | Lib _ -> true in
      List.filter is_lib w.compilables
  end

  include WorkspaceF
  module WorkspaceFold = Make (WorkspaceF)
  module CompilableFold = Make (Compilable)

  exception CircularDependency = WorkspaceFold.CircularDependency
  exception UnresolvedDependency of Compilable.t

  (* Topological Iterators *)
  let foldCompilables f w x =
    let open Compilable in
    let add_lib s = function Exe _ -> s | Lib l -> S.add l.uid s in
    let lib_set = List.fold_left add_lib S.empty w.compilables in
    let run_exe y c =
      match c with
      | Exe e ->
          if S.subset e.requires lib_set then f c y
          else raise (UnresolvedDependency c)
      | Lib _ -> y
    in
    List.fold_left run_exe (WorkspaceFold.fold f w x) w.compilables

  let fold f w x = foldCompilables (CompilableFold.fold f) w x

  let iter f w =
    let thunk m () = f m in
    fold thunk w ()

  (* Formatters *)
  let format_compilables =
    let pp_sep ppf () = fprintf ppf ";@;" in
    fun ppf ->
      fprintf ppf "compilables: [@;@[<v>%a@]]"
        (pp_print_list ~pp_sep Compilable.fprint)

  let fprint ppf w =
    let format_root ppf = fprintf ppf "root: %s" w.root in
    let format_context ppf = fprintf ppf "context: %s" w.context in
    fprintf ppf "{@[<v> Workspace@  %t@  %t@  @[%a@]@]@;}" format_root
      format_context format_compilables w.compilables
end
