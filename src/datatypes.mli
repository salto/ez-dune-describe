(** Copyright © Inria 2022

    @author Pierre Lermusiaux <pierre.lermusiaux@inria.fr> *)

type set = Set.Make(String).t

(* Formatters *)

(** Module type of formattable datatypes providing a formatting function for the
    datatype *)
module type FORMATTABLE = sig
  type t

  val fprint : Format.formatter -> t -> unit
  (** formatting function for the type t *)
end

(** Module type of dependable units. A dependable unit should have a unique
    identifier (within the scope) and a collection of its own dependancies *)
module type DEPENDABLE = sig
  type t

  val get_id : t -> string
  (** [get_id d] returns a unique identifier (within its scope) of the
      dependable d *)

  val get_deps : t -> set
  (** [get_deps d] returns the set of identifiers of the dependancies of d *)
end

(** Module type of foldable units providing a function to produce a list of the
    dependable units it is dependant on *)
module type FOLDABLE = sig
  module Dep : DEPENDABLE

  type t

  val get_fold_list : t -> Dep.t list
  (** [get_fold_list f] returns the list of dependables of f *)
end

(* Datatypes *)

(** Module of OCaml modules as described by dune. Modules are both formattable
    and dependable *)
module Module : sig
  type t =
    { name : string;
      impl : string;
      intf : string;
      cmt : string;
      cmti : string;
      impl_deps : set;
      intf_deps : set
    }

  include FORMATTABLE with type t := t
  include DEPENDABLE with type t := t
end

(** Module of dune compilable units (only Executables and Libraries).
    Compilables are formattable, dependable and foldable over their module
    dependancies. NB: Executables do not have a unique identifier, therefore
    need to be folded separately *)
module Compilable : sig
  type t =
    | Exe of
        { names : string list;
          requires : set;
          modules : Module.t list;
          include_dirs : string list
        }
    | Lib of
        { name : string;
          uid : string;
          local : bool;
          requires : set;
          source_dir : string;
          modules : Module.t list;
          include_dirs : string list
        }

  include FORMATTABLE with type t := t
  include DEPENDABLE with type t := t
  include FOLDABLE with type t := t and module Dep = Module
end

(* Topological iterators *)

(** Module type providing a fold (and iter) function for a foldable type *)
module type FOLD = sig
  type t
  type d

  exception CircularDependency
  (** Exception to idendify unresolved dependencies, most likely caused by a
      circular dependency *)

  val fold : (d -> 'a -> 'a) -> t -> 'a -> 'a
  (** [fold f w acc] folds f over the compilable units of the workspace w in the
      topological order of the dependencies *)

  val iter : (d -> unit) -> t -> unit
  (** [iter f w acc] iters f over the compilable units of the workspace w in the
      topological order of the dependencies *)

  (*
   * val fold_inv : (d -> 'a -> 'a) -> t -> 'a -> 'a
   * val iter_inv : (d -> unit) -> t -> unit
   *)
end

(** Module of dune workspaces. Workspaces are both formattable and foldable over
    their compilable dependancies. NB: dependant Executables do not have a
    unique identifier, therefore need to be folded separately *)
module Workspace : sig
  type t =
    { root : string;
      context : string;
      compilables : Compilable.t list
    }

  include FORMATTABLE with type t := t
  include FOLDABLE with type t := t and module Dep = Compilable
  include FOLD with type t := t and type d := Module.t

  (** Exception to idendify unresolved dependencies, most likely caused by a
      circular dependency *)
  val foldCompilables : (Compilable.t -> 'a -> 'a) -> t -> 'a -> 'a
  (** [fold f w acc] folds f over the libraries of the workspace w in the
      topological order of the dependencies first, and then fold over the
      executables, checking that all their dependencies are libraries handled
      previously *)

  exception UnresolvedDependency of Compilable.t
  (** Exception to idendify Executables with unresolved dependencies after
      folding over the libraries *)
end

(** Functor parametrized over the foldable type considered, producing the
    corresponding fold (and iter) function. Can thus be used both for Workspaces
    and Compilables *)
module Make (X : FOLDABLE) : FOLD with type t := X.t and type d := X.Dep.t
