(** Copyright © Inria 2022

    @author Pierre Lermusiaux <pierre.lermusiaux@inria.fr> *)

open Sexp_decode
open Datatypes

(** Module type providing a S-expression decoder for the dune description of a
    given datatype *)
module type S = sig
  type t

  val decoder : t decoder
  (** S-expression decoder of the dune description *)
end

module WorkspaceDecoder : S with type t := Workspace.t

val workspace_decoder : Workspace.t decoder
(** S-expression_decoder for the dune describe command. Must be used with
    format=csexp, and can be used with --with-deps option *)
