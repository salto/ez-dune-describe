(** Copyright © Inria 2022

    @author Pierre Lermusiaux <pierre.lermusiaux@inria.fr>
    @author Benoît Montagu <benoit.montagu@inria.fr> *)

open Ez_dune_describe
open Datatypes
open Format

let usage =
  "Usage: dune_fold FILE\n\
   Decodes the file FILE, that must contain an S-expression describing a valid \
   dune workspace, and prints on the standard output channel the names of \
   executables and library identifiers in an order that is compatible with the \
   topological ordering of the dune project's dependencies."

let () =
  if Array.length Sys.argv <> 2 then (
    output_string stderr "dune_fold expects 1 argument.\n";
    output_string stderr usage;
    output_char stderr '\n';
    flush_all ();
    exit 1 )

let s =
  let chn = open_in Sys.argv.(1) in
  let s = input_line chn in
  close_in chn;
  s

let print_ids cl =
  let open Compilable in
  let sep ppf () = fprintf ppf ";@\n" in
  let print_id ppf =
    let sep_names ppf () = fprintf ppf ",@ " in
    function
    | Lib l -> fprintf ppf "@[%s@]" l.uid
    | Exe e ->
        fprintf ppf "@[%a@]"
          (pp_print_list ~pp_sep:sep_names pp_print_string)
          e.names
  in
  printf "@[%a@]@." (pp_print_list ~pp_sep:sep print_id) cl

let () =
  match Csexp.parse_string s with
  | Ok cse ->
      ( match Sexp_decode.run Decoder.workspace_decoder cse with
      | Some w -> print_ids (Workspace.foldCompilables List.cons w [])
      | None -> assert false )
  | Error _ -> assert false
