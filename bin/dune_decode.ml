(** Copyright © Inria 2022

    @author Pierre Lermusiaux <pierre.lermusiaux@inria.fr>
    @author Benoît Montagu <benoit.montagu@inria.fr> *)

open Ez_dune_describe
open Datatypes

let usage =
  "Usage: dune_decode FILE\n\
   Decodes the file FILE, that must contain an S-expression describing a valid \
   dune workspace, and prints its contents on the standard output channel."

let () =
  if Array.length Sys.argv <> 2 then (
    output_string stderr "dune_decode expects 1 argument.\n";
    output_string stderr usage;
    output_char stderr '\n';
    flush_all ();
    exit 1 )

let s =
  let chn = open_in Sys.argv.(1) in
  let s = input_line chn in
  close_in chn;
  s

let () =
  match Csexp.parse_string s with
  | Ok cse ->
      ( match Sexp_decode.run Decoder.workspace_decoder cse with
      | Some w -> Format.printf "@[%a@]@." Workspace.fprint w
      | None -> print_endline "Error while decoding csexp" )
  | Error _ -> print_endline "Not a csexp"
